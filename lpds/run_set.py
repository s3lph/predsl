import string
from random import randint, choice

from lpds.ds.set import SetRO, SetRW
from lpds.log.memory import InMemoryAppendOnlyLog

from matplotlib import pyplot as plot
from tqdm import tqdm


if __name__ == '__main__':

    log = InMemoryAppendOnlyLog()
    log1 = InMemoryAppendOnlyLog(replicate=log)

    ds0 = SetRW(log)

    for _ in tqdm(range(10000)):
        if randint(0, 2) == 0 and len(ds0) > 0:
            k = choice(list(ds0))
            ds0.remove(k)
        else:
            k = randint(0, 100000)
            ds0.add(k)

    log2 = InMemoryAppendOnlyLog(replicate=log)

    for _ in tqdm(range(10000)):
        if randint(0, 2) == 0 and len(ds0) > 0:
            k = choice(list(ds0))
            ds0.remove(k)
        else:
            k = randint(0, 100000)
            ds0.add(k)

    log3 = InMemoryAppendOnlyLog(replicate=log)

    ds1 = log1.get_datastructure(0)
    ds2 = log2.get_datastructure(0)
    ds3 = log3.get_datastructure(0)

    print(ds0.oldest())

    print(len(ds0))
    print(min(ds0._key_serials.values()) if len(ds0._key_serials.values()) > 0 else None)
    print(set(ds0) == set(ds1), ds1.consumed)
    print(set(ds0) == set(ds2), ds2.consumed)
    print(set(ds0) == set(ds3), ds3.consumed)

    # plot.hist(ds0._key_serials.values(), bins=25, range=(0, len(log)))
    # plot.axvline(log.oldest(), color='red')
    # plot.show()
    xs = []
    ys = []
    cs = []
    s = []
    for i in range(log.oldest()):
        xs.append(i)
        ys.append(i % 150)
        s.append(4)
        cs.append('red')
    for i in log.sliding_window:
        if i not in ds0._serials.values():
            xs.append(i)
            ys.append(i % 150)
            s.append(4)
            cs.append('yellow')
    for i in log.sliding_window:
        if i in ds0._serials.values():
            xs.append(i)
            ys.append(i % 150)
            s.append(4)
            cs.append('blue')
    plot.scatter(xs, ys, s=s, c=cs)
    plot.show()
