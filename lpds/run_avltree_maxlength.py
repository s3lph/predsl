import string
from random import randint, choice

from lpds.ds.avltree import AVLTreeRO, AVLTreeRW
from lpds.log.memorymaxlength import InMemoryMaxLengthAppendOnlyLog

from matplotlib import pyplot as plot
from tqdm import tqdm


if __name__ == '__main__':

    log = InMemoryMaxLengthAppendOnlyLog(max_length=12000)
    log1 = InMemoryMaxLengthAppendOnlyLog(replicate=log)

    ds0 = AVLTreeRW(log)

    for _ in tqdm(range(10000)):
        if randint(0, 3) == 0 and len(ds0) > 0:
            k = choice(list(ds0._nodes.values()))
            ds0.delete(k.value)
        else:
            k = randint(0, 100000)
            if ds0.search(k) is None:
                ds0.insert(k)
            else:
                ds0.delete(k)

    log2 = InMemoryMaxLengthAppendOnlyLog(replicate=log)

    for _ in tqdm(range(100000)):
        if len(ds0) > 0 and (randint(0, 3) == 0 or len(log.sliding_window) >= 0.5 * log._max_length):
            k = choice(list(ds0._nodes.values()))
            ds0.delete(k.value)
        else:
            k = randint(0, 100000)
            if ds0.search(k) is None:
                ds0.insert(k)
            else:
                ds0.delete(k)

    log3 = InMemoryMaxLengthAppendOnlyLog(replicate=log)

    ds1 = log1.get_datastructure(0)
    ds2 = log2.get_datastructure(0)
    ds3 = log3.get_datastructure(0)

    print(ds0.oldest())

    print(len(ds0))
    print(ds0 == ds1, ds1.consumed)
    print(ds0 == ds2, ds2.consumed)
    print(ds0 == ds3, ds3.consumed)
    print(log.stats[-1][2])  # performance

    plot.plot(log.stats)
    plot.hlines([0.5, 1], colors=['forestgreen', 'red'], xmin=0, xmax=len(log))
    plot.ylim(bottom=0)
    plot.show()

    plot.plot([x[0]/x[1] if x[1] > 0 else 1 for x in log.stats])
    plot.plot([x[2] for x in log.stats], color='purple')
    plot.hlines([0.5, 1], colors=['forestgreen', 'red'], xmin=0, xmax=len(log))
    plot.ylim(bottom=0, top=4)
    plot.show()

    # plot.hist(ds0._nodes.keys(), bins=25, range=(0, len(log)))
    # plot.axvline(log.oldest(), color='red')
    xs = []
    ys = []
    cs = []
    s = []
    for i in range(log.oldest()):
        xs.append(i)
        ys.append(i % 150)
        s.append(4)
        cs.append('red')
    for i in log.sliding_window:
        if i not in ds0._nodes.keys():
            xs.append(i)
            ys.append(i % 150)
            s.append(4)
            cs.append('yellow')
    for i in log.sliding_window:
        if i in ds0._nodes.keys():
            xs.append(i)
            ys.append(i % 150)
            s.append(4)
            cs.append('blue')
    plot.scatter(xs, ys, s=s, c=cs)
    plot.show()

    xs = []
    ys = []
    cs = []
    for i in range(1, len(log.stats)):
        xs.append(i)
        ys.append(log.stats[i][3])
        cs.append('blue' if log.stats[i][4] == 1 else 'red')
    plot.scatter(xs, ys, s=.1, c=cs)
    plot.show()

