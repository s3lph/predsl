
import string
from random import randint, choice

from lpds.ds.graph import DirectedWeightedGraphRW, DirectedWeightedGraphRO
from lpds.log.memory import InMemoryAppendOnlyLog

from matplotlib import pyplot as plot
from tqdm import tqdm


if __name__ == '__main__':

    log = InMemoryAppendOnlyLog()
    log1 = InMemoryAppendOnlyLog(replicate=log)

    ds0 = DirectedWeightedGraphRW(log)

    for _ in tqdm(range(10000)):
        action = randint(0, 3)
        if action == 0 and len(ds0) > 0:
            k = choice(ds0.nodes)
            ds0.remove(k)
        elif action == 1 and len(ds0) > 1:
            k1 = choice(ds0.nodes)
            k2 = choice(ds0.nodes)
            if (k1, k2) in ds0:
                ds0.unlink(k1, k2)
            else:
                ds0.link(k1, k2, randint(0, 100))
        else:
            v = choice(string.ascii_letters)
            if len(ds0) > 0:
                k = choice(ds0.nodes)
            n = ds0.add(v)
            if len(ds0) > 1:
                ds0.link(n, k, randint(0, 100))

    log2 = InMemoryAppendOnlyLog(replicate=log)

    for _ in tqdm(range(10000)):
        action = randint(0, 3)
        if action == 0 and len(ds0) > 0:
            k = choice(ds0.nodes)
            ds0.remove(k)
        elif action == 1 and len(ds0) > 1:
            k1 = choice(ds0.nodes)
            k2 = choice(ds0.nodes)
            if (k1, k2) in ds0:
                ds0.unlink(k1, k2)
            else:
                ds0.link(k1, k2, randint(0, 100))
        else:
            v = choice(string.ascii_letters)
            if len(ds0) > 0:
                k = choice(ds0.nodes)
            n = ds0.add(v)
            if len(ds0) > 1:
                ds0.link(n, k, randint(0, 100))

    log3 = InMemoryAppendOnlyLog(replicate=log)

    ds1 = log1.get_datastructure(0)
    ds2 = log2.get_datastructure(0)
    ds3 = log3.get_datastructure(0)

    print(ds0.oldest())

    print(len(ds0), len(ds0.edges), len(ds0.edges) / len(ds0))
    print(min(ds0.nodes) if len(ds0) > 0 else None)
    print(ds0 == ds1, ds1.consumed)
    print(ds0 == ds2, ds2.consumed)
    print(ds0 == ds3, ds3.consumed)

    plot.hist(ds0.nodes, bins=25, range=(0, len(log)))
    plot.axvline(log.oldest(), color='red')
    plot.show()
    # ds0.graphviz().render()  # digraph with so many nodes takes ages to render
