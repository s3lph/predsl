import string
from random import randint, choice

from lpds.ds.linkedlist import LinkedListRO, LinkedListRW
from lpds.log.memory import InMemoryAppendOnlyLog

from matplotlib import pyplot as plot
from tqdm import tqdm


log = InMemoryAppendOnlyLog(strategy='L')
ds0 = LinkedListRW(log)
verify = []

log1 = InMemoryAppendOnlyLog(replicate=log)

# ds42 = LinkedListRW(log, 1) # this should not end up in ds1,2,3
# ds42.insert(0, 'ThisShouldNotBeHere')
# ds42.insert(0, 'ThisShouldNotBeHere')

for i in range(1000):
    x = choice(string.ascii_letters)
    ds0.append(x)
    verify.append(x)

for _ in tqdm(range(10000)):
    r = randint(0, 2)
    if r == 0 and len(ds0) > 0:
        # delete
        i = randint(0, len(ds0) - 1)
        del ds0[i]
        del verify[i]
    elif r == 1 and len(ds0) > 0:
        # replace
        i = randint(0, len(ds0) - 1)
        x = choice(string.ascii_letters)
        ds0[i] = x
        verify[i] = x
    else:
        # insert
        i = randint(0, len(ds0))
        x = choice(string.ascii_letters)
        ds0.insert(i, x)
        verify.insert(i, x)
# sys.exit(0)

# ds42.insert(0, 'ThisShouldNotBeHere')
log2 = InMemoryAppendOnlyLog(replicate=log)
# ds42.insert(0, 'ThisShouldNotBeHere')

for _ in tqdm(range(10000)):
    r = randint(0, 2)
    if r == 0 and len(ds0) > 0:
        # delete
        i = randint(0, len(ds0) - 1)
        del ds0[i]
        del verify[i]
    elif r == 1 and len(ds0) > 0:
        # replace
        i = randint(0, len(ds0) - 1)
        x = choice(string.ascii_letters)
        ds0[i] = x
        verify[i] = x
    else:
        # insert
        i = randint(0, len(ds0))
        x = choice(string.ascii_letters)
        ds0.insert(i, x)
        verify.insert(i, x)

log3 = InMemoryAppendOnlyLog(replicate=log)
# ds42.insert(0, 'ThisShouldNotBeHere')
# ds23 = LinkedListRO(log, 1)
# print(list(ds23))

ds1 = log1.get_datastructure(0)
ds2 = log2.get_datastructure(0)
ds3 = log3.get_datastructure(0)

print(len(ds0))
print(min(ds0._items) if len(ds0._items) > 0 else None)
print('Correct: ' + str(list(ds0) == verify))
print(len(verify), verify)
print(list(ds0) == list(ds1), ds1.consumed)
print(list(ds0) == list(ds2), ds2.consumed)
print(list(ds0) == list(ds3), ds3.consumed)
print(log.stats[-1][2])  # performance

plot.plot(log.stats)
plot.hlines([1, 1.9], colors=['forestgreen', 'red'], xmin=0, xmax=len(log))
plot.ylim(bottom=0)
plot.show()

plot.plot([x[0]/x[1] if x[1] > 0 else 1 for x in log.stats])
plot.plot([x[2] for x in log.stats], color='purple')
plot.hlines([1, 1.9], colors=['forestgreen', 'red'], xmin=0, xmax=len(log))
plot.ylim(bottom=0, top=7)
plot.show()

plot.hist(ds0._items.keys(), bins=25, range=(0, len(log)))
plot.axvline(log.oldest(), color='red')
plot.show()
