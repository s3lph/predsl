
import string
from random import randint, choice

from lpds.ds.graph import GraphRO, GraphRW
from lpds.log.memory import InMemoryAppendOnlyLog

from matplotlib import pyplot as plot
from tqdm import tqdm


if __name__ == '__main__':

    log = InMemoryAppendOnlyLog()
    log1 = InMemoryAppendOnlyLog(replicate=log, preemptive_rewrite=True)

    ds0 = GraphRW(log)

    for _ in tqdm(range(10000)):
        action = randint(0, 3)
        if action == 0 and len(ds0) > 0:
            k = choice(ds0.nodes)
            ds0.remove(k)
        elif action == 1 and len(ds0) > 1:
            k1 = choice(ds0.nodes)
            k2 = choice(ds0.nodes)
            if (k1, k2) in ds0:
                ds0.unlink(k1, k2)
            else:
                ds0.link(k1, k2)
        else:
            v = choice(string.ascii_letters)
            if len(ds0) > 0:
                k = choice(ds0.nodes)
            n = ds0.add(v)
            if len(ds0) > 1:
                ds0.link(n, k)

    log2 = InMemoryAppendOnlyLog(replicate=log)

    for _ in tqdm(range(10000)):
        action = randint(0, 3)
        if action == 0 and len(ds0) > 0:
            k = choice(ds0.nodes)
            ds0.remove(k)
        elif action == 1 and len(ds0) > 1:
            k1 = choice(ds0.nodes)
            k2 = choice(ds0.nodes)
            if (k1, k2) in ds0:
                ds0.unlink(k1, k2)
            else:
                ds0.link(k1, k2)
        else:
            v = choice(string.ascii_letters)
            if len(ds0) > 0:
                k = choice(ds0.nodes)
            n = ds0.add(v)
            if len(ds0) > 1:
                ds0.link(n, k)

    log3 = InMemoryAppendOnlyLog(replicate=log)

    ds1 = log1.get_datastructure(0)
    ds2 = log2.get_datastructure(0)
    ds3 = log3.get_datastructure(0)

    print(ds0.oldest())

    print(len(ds0), len(ds0.edges), len(ds0.edges) / len(ds0))
    print(min(ds0.nodes) if len(ds0) > 0 else None)
    print(ds0 == ds1, ds1.consumed)
    print(ds0 == ds2, ds2.consumed)
    print(ds0 == ds3, ds3.consumed)
    print(log.stats[-1][2])  # performance

    plot.plot([x[0] for x in log.stats], color='blue')
    plot.plot([x[1] for x in log.stats], color='green')
    plot.plot([x[1]*1.9 for x in log.stats], color='red')
    plot.ylim(bottom=0, top=4)
    plot.show()

    plot.plot([x[0]/x[1] if x[1] > 0 else 1 for x in log.stats])
    plot.plot([x[2] for x in log.stats], color='purple')
    plot.hlines([1, 1.9], colors=['forestgreen', 'red'], xmin=0, xmax=len(log))
    plot.ylim(bottom=0, top=4)
    plot.show()

    plot.hist(ds0.nodes, bins=25, range=(0, len(log)))
    plot.axvline(log.oldest(), color='red')
    plot.show()
    ds0.graphviz().render()
