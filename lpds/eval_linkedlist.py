import json
import statistics
import string
import sys
import time
from random import Random

from lpds.ds import AVLTreeRW, DictionaryRW, LinkedListRW, SetRW, GraphRW
from lpds.log.memory import InMemoryAppendOnlyLog

from matplotlib import pyplot as plot
from parfor import pmap


def run_Graph_Random(log):
    r = Random(time.time_ns())
    ds0 = GraphRW(log)
    for _ in range(1000):
        action = r.randint(0, 3)
        if action == 0 and len(ds0) > 0:
            k = r.choice(ds0.nodes)
            ds0.remove(k)
        elif action == 1 and len(ds0) > 1:
            k1 = r.choice(ds0.nodes)
            k2 = r.choice(ds0.nodes)
            if (k1, k2) in ds0:
                ds0.unlink(k1, k2)
            else:
                ds0.link(k1, k2)
        else:
            v = r.choice(string.ascii_letters)
            n = ds0.add(v)
            if len(ds0) > 1:
                k = r.choice(ds0.nodes)
                ds0.link(n, k)


def run_Set_Random(log):
    r = Random(time.time_ns())
    ds0 = SetRW(log)
    for _ in range(1000):
        if r.randint(0, 2) == 0 and len(ds0) > 0:
            k = r.choice(list(ds0))
            ds0.remove(k)
        else:
            k = r.randint(0, 1000)
            ds0.add(k)


def run_Dictionary_Random(log):
    r = Random(time.time_ns())
    ds0 = DictionaryRW(log)
    for _ in range(1000):
        if r.randint(0, 2) == 0 and len(ds0) > 0:
            k = r.choice(list(ds0.keys()))
            del ds0[k]
        else:
            k = r.randint(0, 1000)
            v = r.choice(string.ascii_letters)
            ds0[k] = v


def run_AVLTree_Random(log):
    r = Random(time.time_ns())
    ds0 = AVLTreeRW(log)
    for _ in range(1000):
        if r.randint(0, 3) == 0 and len(ds0) > 0:
            k = r.choice(list(ds0._nodes.values()))
            ds0.delete(k.value)
        else:
            k = r.randint(0, 100000)
            if ds0.search(k) is None:
                ds0.insert(k)
            else:
                ds0.delete(k)


def run_LinkedList_Sorting(log):
    r = Random(time.time_ns())
    ds0 = LinkedListRW(log)
    for _ in range(1000):
        ds0.insert(0, r.randint(0, 500))
    ds0.sort()


def run_LinkedList_Random(log):
    r = Random(time.time_ns())
    ds0 = LinkedListRW(log)
    for i in range(50):
        x = r.choice(string.ascii_letters)
        ds0.append(x)
    for _ in range(1000):
        a = r.randint(0, 2)
        if a == 0 and len(ds0) > 0:
            # delete
            i = r.randint(0, len(ds0) - 1)
            del ds0[i]
        elif a == 1 and len(ds0) > 0:
            # replace
            i = r.randint(0, len(ds0) - 1)
            x = r.choice(string.ascii_letters)
            ds0[i] = x
        else:
            # insert
            i = r.randint(0, len(ds0))
            x = r.choice(string.ascii_letters)
            ds0.insert(i, x)


def run_task(task, *args, **kwargs):
    log = InMemoryAppendOnlyLog(*args, **kwargs)
    task(log)
    return (
        kwargs['strategy'],
        len(log),  # total length of the log
        max((x[0] for x in log.stats)),  # maximum minimal length of the sliding window
        max((x[1] for x in log.stats)),  # maximum actual length of the sliding window
        log.stats[-1][2],  # performance of the log,
        max(log.jstats)  # max length of the sliding window
    )


def main(task):
    strategies = {
        'L': 'baseline',
        'LP': 'merge preemptive',
        'S': 'single',
        'SP': 'single, merge preemptive',
        'B': 'batch',
        'BP': 'batch, merge preemptive',
    }
    if task.__name__ in {'run_Set_Random', 'run_Dictionary_Random'}:
        del strategies['LP']
        del strategies['SP']
        del strategies['BP']
    r = range(10, 50)
    data = []
    for s in strategies:
        print(s)
        data.append(list(pmap(lambda kf: [run_task(task, strategy=s, kf=kf/10.) for _ in range(20)], r)))

    with open(task.__name__ + '.json', 'w') as f:
        json.dump(data, f)

    # sys.exit(0)

    with open(task .__name__ + '.json', 'r') as f:
        data = json.load(f)

    _, ds, workload = task.__name__.split('_', 2)
    title = f'{ds}: {workload}'
    xs = [x/10 for x in r]
    fname = title.replace(':', '').replace(' ', '_').lower()

    # plot.title(f'{title}: Total number of log entries')
    # for i, d in enumerate(data):
    #     if d[0][0][0] not in strategies:
    #         continue
    #     xe = [x + 0.01 * (i - len(data) / 2) for x in xs]
    #     ys = [statistics.mean([y[1] for y in ys]) for ys in d]
    #     ye = [statistics.stdev([y[1] for y in ys]) for ys in d]
    #     plot.errorbar(x=xe, y=ys, yerr=ye, label=strategies[d[0][0][0]], elinewidth=1)
    # plot.xlabel('$k_{max}$')
    # plot.ylabel('Log length / number of entries')
    # plot.ylim(bottom=0)
    # plot.legend(title='Strategies', ncol=2)
    # plot.savefig(f'{fname}_loglen.pdf')
    # plot.show()

    plot.title(f'{title}: Rewrite Efficiency')
    for i, d in enumerate(data):
        if d[0][0][0] not in strategies:
            continue
        xe = [x + 0.01 * (i - len(data) / 2) for x in xs]
        ys = [statistics.mean([y[4] for y in ys]) for ys in d]
        ye = [statistics.stdev([y[4] for y in ys]) for ys in d]
        plot.errorbar(x=xe, y=ys, yerr=ye, label=strategies[d[0][0][0]], elinewidth=1)
    plot.xlabel('$k_{max}$')
    plot.ylabel('Rewrite Efficiency')
    plot.ylim(bottom=0)
    # plot.legend(title='Strategies', ncol=2)
    plot.savefig(f'{fname}_rweff.pdf')
    plot.show()

    plot.title(f'{title}: Maximal sliding window size')
    for i, d in enumerate(data):
        if d[0][0][0] not in strategies:
            continue
        xe = [x + 0.01 * (i - len(data) / 2) for x in xs]
        ys = [statistics.mean([y[5] for y in ys])/1000 for ys in d]
        ye = [statistics.stdev([y[5] for y in ys])/1000 for ys in d]
        plot.errorbar(x=xe, y=ys, yerr=ye, label=strategies[d[0][0][0]], elinewidth=1)
    plot.xlabel('$k_{max}$')
    plot.ylabel('Sliding window size / kB, JSON-serialized')
    plot.ylim(bottom=0)
    plot.legend(title='Strategies', ncol=2)
    plot.savefig(f'{fname}_msws.pdf')
    plot.show()

    # plot.title(f'{title}: Log Overhead')
    # for i, d in enumerate(data):
    #     if d[0][0][0] not in strategies:
    #         continue
    #     xe = [x + 0.01 * (i - len(data) / 2) for x in xs]
    #     ys = [statistics.mean([y[2]/y[3] for y in ys]) for ys in d]
    #     ye = [statistics.stdev([y[2]/y[3] for y in ys]) for ys in d]
    #     plot.errorbar(x=xe, y=ys, yerr=ye, label=strategies[d[0][0][0]], elinewidth=1)
    # plot.xlabel('$k_{max}$')
    # plot.ylabel('Log overhead $k$')
    # plot.ylim(bottom=0)
    # plot.legend(title='Strategies', ncol=2)
    # plot.savefig(f'{fname}_overhead.pdf')
    # plot.show()


if __name__ == '__main__':
    main(run_LinkedList_Random)
    main(run_LinkedList_Sorting)
    main(run_Dictionary_Random)
    main(run_Set_Random)
    main(run_Graph_Random)
    main(run_AVLTree_Random)
