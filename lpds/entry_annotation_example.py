import json
import math
import statistics
import string
import sys
import time
from random import Random

from lpds.ds import AVLTreeRW, DictionaryRW, LinkedListRW, SetRW, GraphRW
from lpds.log.memory import InMemoryAppendOnlyLog

from matplotlib import pyplot as plot


def annotate_ll(log, ds: LinkedListRW):
    sw: range = log.sliding_window
    relevance = {i: 1 for i in sw}
    for i in reversed(sw):
        if 'head' in log[i]:
            relevance[i] = max(relevance.get(i, 0), 2)
            break
    for i in reversed(sw):
        if 'oldest' in log[i]:
            relevance[i] = max(relevance.get(i, 0), 2)
            break
    node = ds._items[ds._head]
    while node is not None:
        relevance[node.serial] = max(relevance.get(node.serial, 0), 3)
        ll = [node.left, node.serial]
        rl = [node.serial, node.right]
        found = 0
        for i in reversed(range(node.serial, sw.stop)):
            if ll in log[i]['links']:
                found += 1
                relevance[i] = max(relevance.get(i, 0), 2)
            if rl in log[i]['links']:
                found += 1
                relevance[i] = max(relevance.get(i, 0), 2)
            if found >= 2:
                break
        if node.right is None:
            break
        node = ds._items[node.right]
    return len(log), sw, relevance, list(ds)


def annotate_linkedlist_random(log):
    annotated = []
    delimiters = []
    def wrap_append(fn):
        def appendoverwrite(entry: dict):
            ret = fn(entry)
            annotated.append(annotate_ll(log, ds0))
            return ret
        return appendoverwrite
    def wrap_prune(fn):
        def pruneoverwrite():
            ret = fn()
            annotated[-1] = annotate_ll(log, ds0)
            return ret
        return pruneoverwrite
    log._append = wrap_append(log._append)
    log._prune = wrap_prune(log._prune)
    r = Random(time.time_ns())
    ds0 = LinkedListRW(log)
    for i in range(20):
        x = r.choice(string.ascii_letters)
        ds0.append(x)
    delimiters.append((log.oldest(), len(log)))
    actions = [2, 1, 0, 2, 2, 1, 0, 1, 1, 1, 0, 1, 0, 2, 1, 0, 1, 1, 0, 0, 2, 2, 2, 0, 1, 0, 2, 0, 1, 0, 2, 0, 1, 2, 1, 1, 2, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 2, 1, 1, 2, 0, 1, 0, 0, 0, 0, 2, 2, 2, 1, 1, 0, 1, 1, 0, 2, 0, 0, 1, 2, 2, 1, 1, 1, 1]
    actionis = [6, 11, 18, 6, 21, 2, 1, 10, 13, 17, 18, 19, 5, 0, 1, 7, 9, 15, 14, 15, 7, 18, 12, 7, 2, 1, 0, 11, 14, 15, 7, 11, 14, 10, 10, 12, 3, 1, 3, 12, 3, 2, 1, 11, 5, 4, 11, 6, 3, 0, 0, 5, 6, 1, 2, 7, 9, 7, 7, 12, 10, 10, 2, 11, 12, 1, 7, 0, 9, 2, 9, 6, 3, 6, 5, 8, 9, 8, 2, 6]
    for a in actions:
        # a = r.randint(0, 2)
        if a == 0 and len(ds0) > 0:
            # delete
            # i = r.randint(0, len(ds0) - 1)
            i = actionis.pop(0)
            del ds0[i]
        elif a == 1 and len(ds0) > 0:
            # replace
            # i = r.randint(0, len(ds0) - 1)
            i = actionis.pop(0)
            x = r.choice(string.ascii_letters)
            ds0[i] = x
        else:
            # insert
            #i = r.randint(0, len(ds0))
            i = actionis.pop(0)
            x = r.choice(string.ascii_letters)
            ds0.insert(i, x)
    # delimiters.append((log.oldest(), len(log)))
    # ds0.sort()
    return annotated, delimiters


def main():
    strategies = {
        '0': 'no pruning',
        'L': 'baseline',
        'LP': 'baseline, preemptive',
        'LQ': 'baseline, split preemptive',
        'S': 'single',
        'SP': 'single, preemptive',
        'SQ': 'single, split preemptive',
        'B': 'batch',
        'BP': 'batch, preemptive',
        'BQ': 'batch, split preemptive',
    }
    log = InMemoryAppendOnlyLog(strategy='BP')
    annotated, delimiters = annotate_linkedlist_random(log)
    xs = []
    ys = []
    colors = {
        0: 'red',
        1: 'grey',
        2: 'orange',
        3: 'blue'
    }
    cs = []
    maxwidth = max((sw.stop-sw.start for _, sw, *_ in annotated))
    svg = f'''
    <svg xmlns="http://www.w3.org/2000/svg" width="{maxwidth*math.sqrt(2)/2*10+80}" height="{(len(log)+1)*math.sqrt(2)*10+80}">
      <defs>
        <marker
          id="triangle"
          viewBox="0 0 10 10"
          refX="1"
          refY="5"
          markerUnits="strokeWidth"
          markerWidth="5"
          markerHeight="5"
          orient="auto">
          <path d="M 0 0 L 10 5 L 0 10 z" />
        </marker>
      </defs>
    <style>
    .relevance0 {{ fill: red; }}
    .relevance1 {{ fill: grey; }}
    .relevance2 {{ fill: orange; }}
    .relevance3 {{ fill: blue; }}
    line.rule {{ stroke: black; opacity: 0.2; }}
    line.major {{ stroke-width: 3px; }}
    line.minor {{ stroke-width: 1.5px; }}
    line.delimiter {{ stroke: red; stroke-width: 1px; }}
    text {{ font-size: 10px; }}
    text.sw-start {{ text-anchor: end; }}
    #root {{ transform: translate({maxwidth*math.sqrt(2)/2*10+40}px, 40px) rotate(45deg); }}
    #legend {{ transform: translate(120px, 40px) rotate(45deg); }}
    #yaxis {{ transform: rotate(-90deg); font-size: 20px; }}
    #xaxis {{ font-size: 20px; }}
    .arrow {{ stroke: black; stroke-width: 2px; marker-end: url(#triangle); }}
    </style>
    <g id="legend">
    <text id="yaxis" x="-60" y="-5">Time</text>
    <line class="arrow" y2="80" />
    <text id="xaxis" y="-5" x="20">Log</text>
    <line class="arrow" x2="80" />
    </g>
    <g id="root">
    '''
    for i in range(0, len(annotated), 10):
        cls = 'minor' if i % 50 else 'major'
        svg += f'<line class="rule {cls}" x1="{i*10}" y1="{i*10-30}" x2="{i*10}" y2="{len(annotated)*10+30}"/>'
    for l, r in delimiters:
        svg += f'<line class="delimiter" x1="{l*10-40}" y1="{r*10+9}" x2="{r*10+40}" y2="{r*10+9}"/>'

    for length, sw, relevant, dsdump in annotated:
        # for x in range(sw.start):
        #     xs.append(x)
        #     ys.append(length)
        #     cs.append(colors[0])
        #    svg += f'<rect x="{x}" y="{length}" width="1" height="0.8" class="relevance0">'
        for k, v in relevant.items():
            xs.append(k)
            ys.append(length)
            cs.append(colors[v])
            svg += f'<rect x="{k*10}" y="{length*10}" width="10" height="8" class="relevance{v}"/>'
        svg += f'<text class="sw-end" x="{length*10+3}" y="{length*10+6}">{length}</text>'
        svg += f'<text class="sw-start" x="{sw.start*10-3}" y="{length*10+6}">{sw.start}</text>'
    svg += '</g></svg>'
    with open('/tmp/log.svg', 'w') as f:
        f.write(svg)
    plot.scatter(x=xs, y=ys, c=cs, marker='_')
    plot.gca().invert_yaxis()
    # plot.show()


if __name__ == '__main__':
    main()
