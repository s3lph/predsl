from typing import List, Optional

from graphviz import Digraph

from lpds.ds.ds import Datastructure


class TreeNode:

    def __init__(self, serial: int):
        self.serial = serial
        self.value = None
        self.parent = None
        self._children = []

    def __getitem__(self, item):
        return self._children[item]

    def __iter__(self):
        for c in self._children:
            yield c

    def __len__(self):
        return len(self._children)

    def __eq__(self, other):
        if not isinstance(other, TreeNode):
            return False
        if len(self._children) != len(other._children):
            return False
        for i in range(len(self._children)):
            # Recurses through the entire tree
            if self._children[i] != other._children[i]:
                return False
        return True

    def __contains__(self, item):
        return item in self._children

    def __delitem__(self, key):
        del self.children[key]

    def __str__(self):
        return f'<{self.value}, s={self.serial}, p={self.parent.serial if self.parent is not None else None}, c=[{", ".join([str(c) for c in self._children])}]>'

    def __repr__(self):
        return f'<{self.__class__.__name__} v={str(self.value)}, s={str(self.serial)}, p={str(self.parent.serial) if self.parent is not None else "none"}, c=[{", ".join([str(c.serial) if c is not None else "none" for c in self._children])}]>'

    @property
    def children(self):
        return list(self._children)

    @children.setter
    def children(self, children):
        self._children = list(children)

    @property
    def invheight(self):
        if self.parent is None:
            return 0
        return self.parent.invheight + 1

    @property
    def inner(self):
        return len([c for c in self._children if c is not None]) > 0

    @property
    def leaf(self):
        return not self.inner


class TreeRO(Datastructure):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None, subscribe=True):
        super().__init__(log, duid, subscribe)
        self.NODECLASS = TreeNode
        self._log = log
        self.duid = duid
        self.consumed = 0
        self._root = None
        self._nodes = {}
        self.rewrites = {}
        self.record_rewrites = False

    def __str__(self):
        return str(self._root)

    def __repr__(self):
        return f'<{self.__class__.__name__} {str(self._root)}>'

    def consume_entry(self, entry: dict, unsafe: bool = False):
        if entry['duid'] != self.duid:
            return
        self.consumed += 1
        serial = entry['serial']
        node = None
        if 'value' in entry:
            node = self.NODECLASS(serial)
        if 'value' in entry:
            value = entry.get('value')
            if value['type'] == 'immediate':
                value = value['value']
            else:
                value = self._log.get_datastructure(value['duid'])
            node.value = value
            self._nodes[serial] = node
        if 'children' in entry:
            for pid, children in entry['children'].items():
                if pid not in self._nodes and unsafe:
                    continue
                parent: TreeNode = self._nodes[pid]
                for nid in parent.children:
                    if nid is not None and not (unsafe and isinstance(nid, int)):
                        nid.parent = None
            for pid, children in entry['children'].items():
                parent: TreeNode = self._nodes.get(pid)
                for nid in children:
                    if nid is not None and nid in self._nodes:
                        if self._nodes[nid].parent is not None and not (unsafe and isinstance(self._nodes[nid].parent, int)):
                            self._nodes[nid].parent.children = [c if c is not None and (isinstance(c, int) or c.serial != nid) else None for c in self._nodes[nid].parent.children]
                        self._nodes[nid].parent = parent if parent is not None else pid
                    if parent is not None and not (unsafe and isinstance(parent, int)):
                        parent.children = [self._nodes[c] if c in self._nodes else c if c is not None else None for c in children]
        if 'root' in entry:
            if entry['root'] is None:
                self._root = None
            else:
                if entry['root'] not in self._nodes and unsafe:
                    self._root = entry['root']
                else:
                    self._root: TreeNode = self._nodes[entry['root']]
                    self._root.parent = None
        # Remove obsolete nodes
        if not unsafe:
            self._nodes = {n.serial: n for n in self}

    def _ppos(self, node: TreeNode):
        if node.parent is None:
            return None, None
        parent = node.parent
        for ppos, child in enumerate(parent.children):
            if child is not None and child.serial == node.serial:
                return parent, ppos
        raise RuntimeError('Node not a child of its parent')

    def rewrite(self, serial: int, entry: dict = None) -> dict:
        if serial not in self._nodes:
            return entry
        node = self._nodes[serial]
        myid = len(self._log)
        if entry is not None:
            # Don't rewrite if the node to be rewritten is being moved around
            if serial in entry['children']:
                return entry
            for v in entry['children'].values():
                if serial in v:
                    return entry
        else:
            entry = self.ref_dict()
            entry['serial'] = myid
            entry['children'] = {}
        if node.parent is None:
            entry['root'] = myid
        else:
            parent, ppos = self._ppos(node)
            entry['children'][parent.serial] = [c.serial if c is not None else None for c in parent.children]
            entry['children'][parent.serial][ppos] = myid
        entry['children'][myid] = [c.serial if c is not None else None for c in node.children]
        entry['value'] = self.wrap(node.value)
        if self.record_rewrites:
            self.rewrites[serial] = myid
        return entry

    def oldest(self, *, older_than: int = -1) -> int:
        self._nodes = {n.serial: n for n in self}
        return min((x for x in self._nodes.keys() if x > older_than), default=older_than)

    def ref_dict(self) -> dict:
        return {
            'type': 'tree',
            'duid': self.duid
        }

    def __len__(self):
        return len(self._nodes)

    def __eq__(self, other):
        if not isinstance(other, TreeRO):
            return False
        if self._root is None:
            return other._root is None
        elif other._root is None:
            return False
        else:
            # Recurses throught the entire tree
            return self._root == other._root

    def __iter__(self):
        if self._root is None:
            return
        stack = [self._root]
        while len(stack) > 0:
            node = stack.pop()
            for c in node.children[::-1]:
                if c is None:
                    continue
                stack.append(c)
            yield node

    @property
    def root(self):
        return self._root

    def graphviz(self, render_null=False):
        dot = Digraph()
        for node in self:
            dot.node(str(node.serial), str(node.value))
            for i, c in enumerate(node.children):
                if c is None and render_null:
                    nullnode = str(node.serial) + 'nullnode' + str(i)
                    dot.node(nullnode, '', shape='box')
                    dot.edge(str(node.serial), nullnode)
                elif c is not None:
                    dot.edge(str(node.serial), str(c.serial))
        return dot


class TreeRW(TreeRO):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None):
        if duid is None:
            duid = log.gen_duid(self)
        super().__init__(log, duid, False)

    def add_node(self, value, children=None, parent=None, ppos=None, replace=False):
        myid = len(self._log)
        entry = self.ref_dict()
        entry['serial'] = myid
        entry['value'] = self.wrap(value)
        entry['children'] = {}
        if parent is None:
            entry['root'] = myid
        else:
            if ppos is None:
                entry['children'][parent.serial] = [myid]
            else:
                before = [c.serial if c is not None else None for c in parent.children[:ppos]]
                if replace:
                    after = [c.serial if c is not None else None for c in parent.children[ppos + 1:]]
                else:
                    after = [c.serial if c is not None else None for c in parent.children[ppos:]]
                entry['children'][parent.serial] = before + [myid] + after
        if children is not None:
            entry['children'][myid] = [c.serial if c is not None else None for c in children]
        self._log.append(entry)

    def clear(self):
        myid = len(self._log)
        entry = self.ref_dict()
        entry['root'] = None
        entry['serial'] = myid
        self._log.append(entry)

    def remove_node(self, node: TreeNode, reparent_children=False, replace_null=False):
        # Removing a tree node is implemented as rewriting the parent node,
        # optionally replacing the to-be-remove node's place by its children
        myid = len(self._log)
        entry = self.ref_dict()
        entry['serial'] = myid
        if node.serial == self._root.serial:
            entry['root'] = None
            self._log.append(entry)
            return
        entry['value'] = self.wrap(node.parent.value)
        entry['children'] = {node.parent.serial: []}
        for c in node.parent.children:
            if c is None:
                entry['children'][node.parent.serial].append(None)
            elif c.serial == node.serial:
                if replace_null:
                    entry['children'][node.parent.serial].append(None)
                elif reparent_children:
                    entry['children'][node.parent.serial].extend([n.serial for n in node.children if n is not None])
            else:
                entry['children'][node.parent.serial].append(c.serial)
        self._log.append(entry)


class BinaryTreeNode(TreeNode):

    def __init__(self, serial: int):
        super().__init__(serial)
        self._children: List[Optional[BinaryTreeNode]] = [None, None]

    @property
    def children(self):
        return list(self._children)

    @children.setter
    def children(self, children):
        if len(children) != 2:
            raise AttributeError('Binary tree must have exactly 2 children per node')
        self._children = list(children)

    @property
    def left(self):
        return self.children[0]

    @left.setter
    def left(self, node):
        self.children[0] = node

    @property
    def right(self):
        return self.children[1]

    @right.setter
    def right(self, node):
        self.children[1] = node

    @property
    def height(self):
        left = self.left.height if self.left is not None else 0
        right = self.right.height if self.right is not None else 0
        return max(left, right) + 1

    @property
    def balance(self):
        left = self.left.height if self.left is not None else 0
        right = self.right.height if self.right is not None else 0
        return right - left

    def inorder(self):
        if self.left is not None:
            yield from self.left.inorder()
        yield self
        if self.right is not None:
            yield from self.right.inorder()

    def postorder(self):
        if self.left is not None:
            yield from self.left.inorder()
        if self.right is not None:
            yield from self.right.inorder()
        yield self

    def preorder(self):
        yield self
        if self.left is not None:
            yield from self.left.inorder()
        if self.right is not None:
            yield from self.right.inorder()
