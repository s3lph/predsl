import math
from typing import List, Optional

from graphviz import Digraph

from lpds.ds.ds import Datastructure
from lpds.ds.tree import TreeNode, TreeRO, TreeRW


class BinaryTreeNode(TreeNode):

    def __init__(self, serial: int):
        super().__init__(serial)
        self._children: List[Optional[BinaryTreeNode]] = [None, None]

    @property
    def children(self):
        return list(self._children)

    @children.setter
    def children(self, children):
        if len(children) != 2:
            raise AttributeError('Binary tree must have exactly 2 children per node')
        self._children = list(children)

    @property
    def left(self):
        return self.children[0]

    @left.setter
    def left(self, node):
        self.children[0] = node

    @property
    def right(self):
        return self.children[1]

    @right.setter
    def right(self, node):
        self.children[1] = node

    @property
    def height(self):
        left = self.left.height if self.left is not None else 0
        right = self.right.height if self.right is not None else 0
        return max(left, right) + 1

    @property
    def balance(self):
        left = self.left.height if self.left is not None else 0
        right = self.right.height if self.right is not None else 0
        return right - left

    def inorder(self):
        if self.left is not None:
            yield from self.left.inorder()
        yield self
        if self.right is not None:
            yield from self.right.inorder()

    def postorder(self):
        if self.left is not None:
            yield from self.left.inorder()
        if self.right is not None:
            yield from self.right.inorder()
        yield self

    def preorder(self):
        yield self
        if self.left is not None:
            yield from self.left.inorder()
        if self.right is not None:
            yield from self.right.inorder()


class BinaryTreeRO(TreeRO):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None, subscribe=True):
        super().__init__(log, duid, subscribe)
        self.NODECLASS = BinaryTreeNode
        self._root: BinaryTreeNode = self._root

    def ref_dict(self):
        return {
            'type': '2tree',
            'duid': self.duid
        }

    def inorder(self):
        if self._root is None:
            return
        return self._root.inorder()

    def balance(self):
        if self._root is None:
            return 0
        return self._root.balance

    def search(self, value):
        pivot = self._root
        while True:
            if pivot is None:
                return None
            elif pivot.value == value:
                return pivot
            elif pivot.value > value:
                pivot = pivot.left
            else:
                pivot = pivot.right


class BinaryTreeRW(TreeRW):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None):
        super().__init__(log, duid)
        self.NODECLASS = BinaryTreeNode

    def ref_dict(self):
        return {
            'type': '2tree',
            'duid': self.duid
        }

    def inorder(self):
        if self._root is None:
            return
        return self._root.inorder()

    def balance(self):
        if self._root is None:
            return 0
        return self._root.balance

    def search(self, value):
        pivot = self._root
        while True:
            if pivot is None:
                return None
            elif pivot.value == value:
                return pivot
            elif pivot.value > value:
                pivot = pivot.left
            else:
                pivot = pivot.right

    def insert(self, value):
        if self._root is None:
            self.add_node(value)
            return
        pivot = self._root
        while True:
            if pivot.value == value:
                raise KeyError('Entry exists')
            elif pivot.value > value:
                if pivot.left is not None:
                    pivot = pivot.left
                    continue
                self.add_node(value, parent=pivot, ppos=0)
                return
            elif pivot.value < value:
                if pivot.right is not None:
                    pivot = pivot.right
                    continue
                self.add_node(value, parent=pivot, ppos=1)
                return

    def delete(self, value):
        n = self.search(value)
        if n is not None:
            self.remove_node(n, reparent_children=True)

    def add_node(self, value, children=None, parent=None, ppos=None, replace=True):
        if not replace:
            raise AttributeError('Binary tree must have exactly 2 children per node')
        if ppos is not None and (ppos < 0 or ppos > 1):
            raise AttributeError('Binary tree must have exactly 2 children per node')
        if children is None:
            children = [None, None]
        if len(children) != 2:
            raise AttributeError('Binary tree must have exactly 2 children per node')
        return super().add_node(value, children, parent, ppos, replace=True)

    def _succ(self, node):
        assert node.right is not None
        succ = node.right
        while succ.left is not None:
            succ = succ.left
        return succ

    def _parent_replace(self, entry, parent, newroot, ppos):
        children = entry.setdefault('children', {})
        if parent is None:
            entry['root'] = newroot.serial if newroot is not None else None
        else:
            if ppos == 0:
                children[parent.serial] = [
                    newroot.serial if newroot is not None else None,
                    parent.right.serial if parent.right is not None else None
                ]
            else:
                children[parent.serial] = [
                    parent.left.serial if parent.left is not None else None,
                    newroot.serial if newroot is not None else None
                ]

    def remove_node(self, node: BinaryTreeNode, reparent_children=False, replace_null=True):
        myid = len(self._log)
        entry = self.ref_dict()
        entry['serial'] = myid
        entry['children'] = {}
        parent, ppos = self._ppos(node)
        if not reparent_children:
            return super().remove_node(node, reparent_children=False, replace_null=True)
        if node.left is None and node.right is None:
            self._parent_replace(entry, parent, node.right, ppos)
            ret = parent
        elif node.left is None:
            self._parent_replace(entry, parent, node.right, ppos)
            ret = node.right
        elif node.right is None:
            self._parent_replace(entry, parent, node.left, ppos)
            ret = node.left
        elif node.right.left is None:
            self._parent_replace(entry, parent, node.right, ppos)
            entry['children'][node.right.serial] = [
                node.left.serial,
                node.right.right.serial if node.right.right is not None else None
            ]
            ret = node.right
        else:
            succ = self._succ(node)
            ret = succ.parent
            self._parent_replace(entry, parent, succ, ppos)
            entry['children'][succ.parent.serial] = [
                succ.right.serial if succ.right is not None else None,
                succ.parent.right.serial if succ.parent.right is not None else None,
            ]
            entry['children'][succ.serial] = [
                node.left.serial,
                node.right.serial
            ]
        self._log.append(entry)
        return ret

    def _rotate_left(self, node: BinaryTreeNode):
        if node is None or node.right is None:
            raise AttributeError('Cannot pivot null nodes')
        parent = node.parent
        new_root: BinaryTreeNode = node.right
        myid = len(self._log)
        entry = self.ref_dict()
        entry['serial'] = myid
        entry['children'] = {
            node.serial: [
                node.left.serial if node.left is not None else None,
                new_root.left.serial if new_root.left is not None else None
            ],
            new_root.serial: [
                node.serial,
                new_root.right.serial if new_root.right is not None else None
            ]
        }
        if parent is not None:
            if parent.left is not None and parent.left.serial == node.serial:
                entry['children'][parent.serial] = [
                    new_root.serial,
                    parent.right.serial if parent.right is not None else None
                ]
            elif parent.right is not None and parent.right.serial == node.serial:
                entry['children'][parent.serial] = [
                    parent.left.serial if parent.left is not None else None,
                    new_root.serial
                ]
        if node.serial == self.root.serial:
            entry['root'] = new_root.serial
        self._log.append(entry)
        return new_root

    def _rotate_right(self, node: BinaryTreeNode):
        if node is None or node.left is None:
            raise AttributeError('Cannot pivot null nodes')
        parent = node.parent
        new_root: BinaryTreeNode = node.left
        myid = len(self._log)
        entry = self.ref_dict()
        entry['serial'] = myid
        entry['children'] = {
            node.serial: [
                new_root.right.serial if new_root.right is not None else None,
                node.right.serial if node.right is not None else None
            ],
            new_root.serial: [
                new_root.left.serial if new_root.left is not None else None,
                node.serial
            ]
        }
        if parent is not None:
            if parent.left is not None and parent.left.serial == node.serial:
                entry['children'][parent.serial] = [
                    new_root.serial,
                    parent.right.serial if parent.right is not None else None
                ]
            elif parent.right is not None and parent.right.serial == node.serial:
                entry['children'][parent.serial] = [
                    parent.left.serial if parent.left is not None else None,
                    new_root.serial
                ]
        if node.serial == self.root.serial:
            entry['root'] = new_root.serial
        self._log.append(entry)
        return new_root

    def _rotate_left_right(self, node):
        if node is None or node.left is None or node.left.right is None:
            raise AttributeError('Cannot pivot null nodes')
        left = node.left
        new_root = left.right
        parent = node.parent
        myid = len(self._log)
        entry = self.ref_dict()
        entry['serial'] = myid
        entry['children'] = {
            new_root.serial: [
                left.serial,
                node.serial
            ],
            left.serial: [
                left.left.serial if left.left is not None else None,
                new_root.left.serial if new_root.left is not None else None,
            ],
            node.serial: [
                new_root.right.serial if new_root.right is not None else None,
                node.right.serial if node.right is not None else None
            ]
        }
        if parent is not None:
            if parent.left is not None and parent.left.serial == node.serial:
                entry['children'][parent.serial] = [
                    new_root.serial,
                    parent.right.serial if parent.right is not None else None
                ]
            elif parent.right is not None and parent.right.serial == node.serial:
                entry['children'][parent.serial] = [
                    parent.left.serial if parent.left is not None else None,
                    new_root.serial
                ]
        if node.serial == self.root.serial:
            entry['root'] = new_root.serial
        self._log.append(entry)
        return new_root

    def _rotate_right_left(self, node):
        if node is None or node.right is None or node.right.left is None:
            raise AttributeError('Cannot pivot null nodes')
        right = node.right
        new_root = right.left
        parent = node.parent
        myid = len(self._log)
        entry = self.ref_dict()
        entry['serial'] = myid
        entry['children'] = {
            new_root.serial: [
                node.serial,
                right.serial
            ],
            right.serial: [
                new_root.right.serial if new_root.right is not None else None,
                right.right.serial if right.right is not None else None
            ],
            node.serial: [
                node.left.serial if node.left is not None else None,
                new_root.left.serial if new_root.left is not None else None,
            ]
        }
        if parent is not None:
            if parent.left is not None and parent.left.serial == node.serial:
                entry['children'][parent.serial] = [
                    new_root.serial,
                    parent.right.serial if parent.right is not None else None
                ]
            elif parent.right is not None and parent.right.serial == node.serial:
                entry['children'][parent.serial] = [
                    parent.left.serial if parent.left is not None else None,
                    new_root.serial
                ]
        if node.serial == self.root.serial:
            entry['root'] = new_root.serial
        self._log.append(entry)
        return new_root

    def _rebalance_node(self, node):
        if node is None:
            return None
        if node.balance < -1:
            if node.left.balance > 0:
                return self._rotate_left_right(node)
            else:
                return self._rotate_right(node)
        elif node.balance > 1:
            if node.right.balance < 0:
                return self._rotate_right_left(node)
            else:
                return self._rotate_left(node)
        return node

    def rebalance(self):
        # Day-Stout-Warren algorithm
        # https://en.wikipedia.org/wiki/Day%E2%80%93Stout%E2%80%93Warren_algorithm
        # Enable rewrite lookup table, needed for the loops below to remain in a consistent state
        self.record_rewrites = True
        # Phase 1: Turn into right linked list
        pivot = self._root
        while pivot is not None:
            if pivot.left is None:
                pivot = pivot.right
            else:
                pivot = self._rotate_right(pivot)
                p = pivot.serial
                while p in self.rewrites:
                    p = self.rewrites[p]
                pivot = self._nodes[p]
        # Clear the rewrite lookup table - the following loop starts out at the root again
        self.rewrites.clear()
        # Phase 2: Rebalance
        self.graphviz(render_null=True).render()
        size = len(self)
        leaves = size + 1 - 2**int(math.log2(size + 1))
        self._rebalance_compress(self._root, leaves)
        size -= leaves
        while size > 1:
            size >>= 1
            self._rebalance_compress(self._root, size)
        # Disable and clear rewrite lookup table
        self.record_rewrites = False
        self.rewrites.clear()

    def _rebalance_compress(self, node, count):
        scanner = node
        for i in range(count):
            if scanner is None or scanner.right is None:
                return
            scanner = self._rotate_left(scanner)
            p = scanner.serial
            while p in self.rewrites:
                p = self.rewrites[p]
            scanner = self._nodes[p]
            scanner = scanner.right
