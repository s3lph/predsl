
from lpds.ds.ds import Datastructure
from lpds.ds.tree import TreeNode
from lpds.ds.binarytree import BinaryTreeNode, BinaryTreeRO, BinaryTreeRW


class AVLTreeRO(BinaryTreeRO):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None, subscribe=True):
        super().__init__(log, duid, subscribe)
        self.NODECLASS = BinaryTreeNode
        self._root: BinaryTreeNode = self._root

    def ref_dict(self):
        return {
            'type': 'avltree',
            'duid': self.duid
        }


class AVLTreeRW(BinaryTreeRW):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None):
        super().__init__(log, duid)
        self.NODECLASS = BinaryTreeNode

    def ref_dict(self):
        return {
            'type': 'avltree',
            'duid': self.duid
        }

    def add_node(self, value, children=None, parent=None, ppos=None, replace=True):
        super().add_node(value, children, parent, ppos, replace=True)
        if parent is None:
            return
        ancestor = parent.parent
        while ancestor is not None:
            ancestor = self._rebalance_node(ancestor)
            ancestor = ancestor.parent

    def remove_node(self, node: BinaryTreeNode, reparent_children=False, replace_null=True):
        ancestor = super().remove_node(node, reparent_children, True)
        while ancestor is not None:
            while ancestor.balance >= 2 or ancestor.balance <= -2:
                ancestor = self._rebalance_node(ancestor)
            ancestor = ancestor.parent

    def rebalance(self):
        pass
