
import abc


class Datastructure(abc.ABC):

    def __init__(self, log, duid: int, subscribe: bool = True):
        pass

    def consume_entry(self, entry: dict, unsafe: bool = False):
        pass

    def rewrite(self, serial: int, entry: dict = None) -> dict:
        pass

    def oldest(self, *, older_than: int = -1) -> int:
        pass

    def ref_dict(self) -> dict:
        pass

    def wrap(self, value):
        if isinstance(value, Datastructure):
            return value.ref_dict()
        else:
            return {
                'type': 'immediate',
                'value': value
            }
