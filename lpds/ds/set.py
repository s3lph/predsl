
from lpds.ds.ds import Datastructure


class SetRO(Datastructure):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None, subscribe=True):
        super().__init__(log, duid, subscribe)
        self._log = log
        self.duid = duid
        if subscribe:
            log.subscribe(self, duid)
        self._set = set()
        self._serials = {}
        self.consumed = 0

    def __iter__(self):
        return self._set.__iter__()

    def consume_entry(self, entry, unsafe: bool = False):
        if entry['duid'] != self.duid:
            return
        self.consumed += 1
        key = entry.get('value')
        if key['type'] == 'immediate':
            key = key['value']
        else:
            key = self._log.get_datastructure(key['duid'])
        if entry.get('delete', False):
            if key is None:
                # delete without key -> clear the dict
                self._set.clear()
                self._serials.clear()
            elif key in self._set:
                # delete with key -> clear the key
                self._set.remove(key)
                del self._serials[key]
        else:
            # key present -> add key
            self._set.add(key)
            self._serials[key] = entry['serial']

    def __contains__(self, k):
        return k in self._set

    def __len__(self):
        return len(self._set)

    def ref_dict(self):
        return {
            'type': 'set',
            'duid': self.duid
        }


class SetRW(SetRO):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None):
        if duid is None:
            duid = log.gen_duid(self)
        super().__init__(log, duid, False)
        self._key_serials = {}

    def rewrite(self, serial: int, entry: dict = None) -> dict:
        keys = [k for k, v in self._key_serials.items() if v == serial]
        if len(keys) == 0:
            return None
        key = keys[0]
        myid = len(self._log)
        if entry is not None:
            if entry['duid'] != self.duid:
                raise RuntimeError('Wrong DUID')
            elif 'value' in entry:
                raise RuntimeError('Entry already has a value')
        else:
            entry = self.ref_dict()
            entry['serial'] = myid
        entry['value'] = self.wrap(key)
        self._key_serials[key] = myid
        return entry

    def add(self, key, /):
        myid = len(self._log)
        entry = self.ref_dict()
        entry['serial'] = myid
        entry['value'] = self.wrap(key)
        self._key_serials[key] = myid
        self._log.append(entry)

    def clear(self):
        myid = len(self._log)
        entry = self.ref_dict()
        entry['serial'] = myid
        entry['delete'] = True
        entry['_do_not_rewrite'] = True
        self._key_serials.clear()
        self._log.append(entry)

    def remove(self, key, /):
        myid = len(self._log)
        entry = self.ref_dict()
        entry['serial'] = myid
        entry['delete'] = True
        entry['value'] = self.wrap(key)
        del self._key_serials[key]
        self._log.append(entry)

    def pop(self):
        if len(self) == 0:
            raise KeyError('set is empty')
        # pop the oldest entry so that the log can automatically be pruned
        topop, _ = min(self._key_serials.items(), key=lambda k, v: v)
        myid = len(self._log)
        entry = self.ref_dict()
        entry['serial'] = myid
        entry['delete'] = True
        entry['value'] = self.wrap(topop)
        del self._key_serials[topop]
        self._log.append(entry)
        return topop

    def oldest(self, *, older_than: int = -1):
        return min((x for x in self._key_serials.values() if x > older_than), default=0)

    def update(self, other: SetRO):
        for k in other:
            if k not in self:
                self.add(k)

    def intersection_update(self, other: SetRO):
        for k in set(self):
            if k not in other:
                self.remove(k)

    def difference_update(self, other: SetRO):
        for k in set(self):
            if k in other:
                self.remove(k)

    def symmetric_difference_update(self, other: SetRO):
        orig_self = set(self)
        for k in orig_self:
            if k in other:
                self.remove(k)
        for k in other:
            if k not in orig_self:
                self.add(k)
