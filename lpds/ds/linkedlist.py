from dataclasses import dataclass
from typing import Optional

from tqdm import tqdm

from lpds.ds.ds import Datastructure


EOL = object()


@dataclass
class DllEntry:
    serial: int
    value: object
    left: Optional[int] = None
    right: Optional[int] = None

    def __eq__(self, other):
        if not isinstance(other, DllEntry):
            return False
        return self.value == other.value

    def __lt__(self, other):
        return self.value < other.value


class LinkedListRO(Datastructure):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = 0, subscribe=True):
        super().__init__(log, duid, subscribe)
        self._log = log
        self.duid = duid
        if subscribe:
            log.subscribe(self, duid)
        self._items = {}
        self._head = None
        self._tail = None
        self.consumed = 0
        self.rewrites = {}
        self.record_rewrites = False

    def __iter__(self):
        if self._head is None:
            return
        x = self._items[self._head]
        while x is not None:
            yield x.value
            x = self._items.get(x.right)

    def consume_entry(self, entry, unsafe=False):
        if entry['duid'] != self.duid:
            return
        self.consumed += 1
        myid = entry.get('serial')
        data = entry.get('value')
        links = entry.get('links')
        if 'head' in entry:
            self._head = entry.get('head')
        if data is not None:
            if data['type'] == 'immediate':
                new = DllEntry(myid, data['value'])
            else:
                new = DllEntry(myid, self._log.get_datastructure(data['duid']))
            self._items[myid] = new
        for link in links:
            left, right = link
            if left is not None:
                if left in self._items or not unsafe:
                    self._items[left].right = right
            if right is not None:
                if right in self._items or not unsafe:
                    self._items[right].left = left
        if not unsafe:
            # Remove obsolete entries
            items = {}
            self._tail = None
            if self._head is not None:
                x = self._items[self._head]
                while x is not None:
                    self._tail = x.serial
                    items[x.serial] = x
                    x = None if x.right is None else self._items[x.right]
            self._items = items

    def _get_backward(self, i, default=None):
        if self._tail is None:
            return default
        if i < 0 or i >= len(self._items):
            return default
        x = self._items[self._tail]
        for j in range(i):
            x = self._items.get(x.left)
        return x

    def _get(self, i, default=None) -> DllEntry:
        if self._head is None:
            return default
        if i < 0 or i >= len(self._items):
            return default
        x = self._items[self._head]
        for j in range(i):
            x = self._items.get(x.right)
        return x

    def get(self, i, default=None):
        if i < 0:
            x = self._get_backward(~i, default)
        else:
            x = self._get(i, default)
        return x.value if x is not None else None

    def __getitem__(self, i):
        if isinstance(i, int):
            return self._get(i).value
        elif isinstance(i, slice):
            node = self._get(i.start)
            j = 0
            while node.right is not None:
                if j % i.step == 0:
                    yield node.value
                node = node.right
                j += 1

    def __len__(self):
        return len(self._items)

    def oldest(self, *, older_than: int = -1):
        items = {}
        self._tail = None
        if self._head is not None:
            x = self._items[self._head]
            while x is not None:
                self._tail = x.serial
                items[x.serial] = x
                x = None if x.right is None else self._items[x.right]
        self._items = items
        return min((x for x in self._items if x > older_than), default=older_than)

    def ref_dict(self):
        return {
            'type': 'linkedlist',
            'duid': self.duid
        }


class LinkedListRW(LinkedListRO):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None):
        if duid is None:
            duid = log.gen_duid(self)
        super().__init__(log, duid, False)
        self._oldest = len(self._log)

    def rewrite(self, serial: int, entry: dict = None) -> dict:
        myid = len(self._log)
        o = self._items.get(serial)
        if o is None:
            return None
        if entry is not None:
            if entry['duid'] != self.duid:
                raise RuntimeError('Wrong DUID')
            elif 'value' in entry:
                raise RuntimeError('Entry already has a value')
            # don't rewrite if item is being modified:
            for left, right in entry['links']:
                if left == serial or right == serial:
                    return entry
        else:
            entry = self.ref_dict()
            entry['serial'] = myid
            entry['links'] = []
        entry['value'] = self.wrap(o.value)
        entry['links'].insert(0, [o.left, myid])
        entry['links'].insert(1, [myid, o.right])
        if self._head == serial and 'head' not in entry:
            entry['head'] = myid
        elif 'head' in entry and entry['head'] == serial:
            entry['head'] = myid
        # self._oldest = min((x for x in self._items if x != serial), default=0)
        if self.record_rewrites:
            self.rewrites[serial] = myid
        return entry

    def insert(self, index: int, value, /):
        myid = len(self._log)
        links = []
        left = self._get(index - 1)
        right = self._get(index)
        entry = {
            'duid': self.duid,
            'type': 'linkedlist',
            'value': self.wrap(value),
            'serial': myid,
            'links': links
        }
        if index == 0:
            entry['head'] = myid
        if left is not None:
            links.append([left.serial, myid])
        if right is not None:
            links.append([myid, right.serial])
        self._log.append(entry)

    def clear(self):
        myid = len(self._log)
        entry = {'duid': self.duid, 'type': 'linkedlist', 'serial': myid, 'links': [], 'head': None, 'oldest': myid}
        self._log.append(entry)

    def __delitem__(self, index):
        if isinstance(index, slice):
            # A contiguous slice can be handled efficiently, if the slice is stepped, we have to iterate
            if index.step is None or index.step == 1:
                first = self._get(index.start)
                last = self._get(index.stop-1)
                rehead = index.start == 0
            else:
                # Reverse s.t. indexes don't get shifted
                for i in reversed(range(index.start, index.stop, index.step)):
                    self.__delitem__(i)
                return
        elif isinstance(index, int):
            first = last = self._get(index)
            rehead = index == 0
        try:
            left = self._items[first.left] if first.left is not None else None
        except:
            pass
        right = self._items[last.right] if last.right is not None else None
        ls = left.serial if left is not None else None
        rs = right.serial if right is not None else None
        entry = {
            'duid': self.duid,
            'type': 'linkedlist',
            'serial': len(self._log),
            'links': [[ls, rs]],
        }
        if rehead:
            entry['head'] = rs
        self._log.append(entry)

    def __setitem__(self, index, value):
        if index < 0 or index >= len(self):
            raise IndexError('Out of bounds')
        left = self._get(index - 1)
        right = self._get(index + 1)
        ls = left.serial if left is not None else None
        rs = right.serial if right is not None else None
        myid = len(self._log)
        entry = {
            'duid': self.duid,
            'type': 'linkedlist',
            'serial': myid,
            'value': self.wrap(value),
            'links': [[ls, myid], [myid, rs]]
        }
        if index == 0:
            entry['head'] = myid
        self._log.append(entry)

    def append(self, obj, /):
        return self.insert(len(self._items), obj)

    def swap(self, a, b):
        """
        Swap two list members by changing links only
        :param a: index of element to swap with b
        :param b: index of element to swap with a
        :return: None
        """
        if a == b:
            return
        myid = len(self._log)
        ai = self._get(a)
        bi = self._get(b)
        al = self._items[ai.left].serial if ai.left is not None else None
        ar = self._items[ai.right].serial if ai.right is not None else None
        bl = self._items[bi.left].serial if bi.left is not None else None
        br = self._items[bi.right].serial if bi.right is not None else None
        if b == a + 1:
            links = [[al, bi.serial], [bi.serial, ai.serial], [ai.serial, br]]
        elif a == b + 1:
            links = [[bl, ai.serial], [ai.serial, bi.serial], [bi.serial, ar]]
        else:
            links = [[bl, ai.serial], [ai.serial, br], [al, bi.serial], [bi.serial, ar]]
        entry = {
            'duid': self.duid,
            'type': 'linkedlist',
            'serial': myid,
            'links': links
        }
        if a == 0:
            entry['head'] = bi.serial
        elif b == 0:
            entry['head'] = ai.serial
        self._log.append(entry)

    def sort(self, key=None, reverse=False):
        """
        Sort the list in-place.
        This implementation uses an implementation of selection sort optimized for use with linked lists.
        Even though selection sort is usually considered to have poor performance (Θ(n²)) it is a good choice
        for this use case because:
          - It guarantees the minimal number of swaps; since each swap creates a log entry this reduces log growth
          - Divide-and-conquer algorithms (e.g. QS, MS) heavily profit from parallelization and random-access, which
            generally performs poor with linked lists.  With selection sort OTOH, linked lists do not introduce
            additional overhead, since all access happens sequentially anyway, and swapping is an O(1) operation.
        :param key: Apply this function to each value when comparing.
        :param reverse: if true, the list will be sorted in descending order.
        :return: None
        """
        def cmp(v):
            value = v.value
            if key is not None:
                value = key(value)
            if reverse:
                value = -value
            return value

        self.record_rewrites = True
        a = self._items[self._head]
        for i in tqdm(range(len(self) - 1)):
            b = self._items[a.right] if a.right is not None else None
            mini = i
            minv = a
            for j in range(i+1, len(self)):
                if cmp(b) < cmp(minv):
                    mini = j
                    minv = b
                b = self._items[b.right] if b.right is not None else None
            if mini > i and cmp(minv) < cmp(a):
                self.swap(i, mini)
                a = minv
            x = a.right
            while x in self.rewrites:
                x = self.rewrites[x]
            a = self._items[x]
        self.record_rewrites = False
        self.rewrites.clear()
