
import unittest

from lpds.ds.tree import TreeRO, TreeRW
from lpds.log.memory import InMemoryAppendOnlyLog


class TestTree(unittest.TestCase):

    def setUp(self) -> None:
        log = InMemoryAppendOnlyLog()
        log2 = InMemoryAppendOnlyLog(replicate=log)
        self._tree = TreeRW(log)
        self._tree.add_node(42)
        self._tree2 = log2.get_datastructure(0)
        self._tree3 = TreeRW(log)
        self._tree3.add_node(42)

    def buildTree(self, ds):
        root = ds.root
        ds.add_node(23, parent=root, ppos=0)
        ds.add_node(7, parent=root, ppos=1)
        n7 = root[1]
        ds.add_node(1337, parent=n7, ppos=0)
        ds.add_node(4, parent=n7, ppos=1)

    def testAddNodes(self):
        self.buildTree(self._tree)
        values = [n.value for n in self._tree]
        values2 = [n.value for n in self._tree2]
        self.assertEqual([42, 23, 7, 1337, 4], values)
        self.assertEqual([42, 23, 7, 1337, 4], values2)

    def testRemoveNode(self):
        self.buildTree(self._tree)
        self._tree.remove_node(self._tree.root[1])
        values = [n.value for n in self._tree]
        values2 = [n.value for n in self._tree2]
        self.assertEqual(1, len(self._tree.root.children))
        self.assertEqual(1, len(self._tree2.root.children))
        self.assertEqual([42, 23], values)
        self.assertEqual([42, 23], values2)

    def testRemoveNodeReparent(self):
        self.buildTree(self._tree)
        self._tree.remove_node(self._tree.root[1], reparent_children=True)
        values = [n.value for n in self._tree]
        values2 = [n.value for n in self._tree2]
        self.assertEqual(3, len(self._tree.root.children))
        self.assertEqual(3, len(self._tree2.root.children))
        self.assertEqual([42, 23, 1337, 4], values)
        self.assertEqual([42, 23, 1337, 4], values2)

    def testClear(self):
        self.buildTree(self._tree)
        self._tree.clear()
        self.assertEqual(0, len(self._tree))
        self.assertEqual(0, len(self._tree2))

    def testEquality(self):
        self.buildTree(self._tree)
        self.buildTree(self._tree3)
        self.assertEqual(self._tree, self._tree2)
        self.assertEqual(self._tree, self._tree3)
        self._tree3.remove_node(self._tree3._root[1][0])
        self.assertNotEqual(self._tree, self._tree3)
        self._tree.remove_node(self._tree._root[1][0])
        self.assertEqual(self._tree2, self._tree3)
        self.assertEqual(self._tree, self._tree3)
