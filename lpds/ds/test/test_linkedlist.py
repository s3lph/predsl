
import unittest

from lpds.ds.linkedlist import LinkedListRW, LinkedListRO
from lpds.log.memory import InMemoryAppendOnlyLog


class TestLinkedList(unittest.TestCase):

    def setUp(self) -> None:
        log = InMemoryAppendOnlyLog()
        log2 = InMemoryAppendOnlyLog(replicate=log)
        self._list = LinkedListRW(log)
        self._list.append('a')
        self._list2 = log2.get_datastructure(0)
        self._list.append('b')
        self._list.append('c')

    def testInsertLeft(self):
        self._list.insert(0, '@')
        self.assertEqual(['@', 'a', 'b', 'c'], list(self._list))
        self.assertEqual(['@', 'a', 'b', 'c'], list(self._list2))

    def testInsertMiddle(self):
        self._list.insert(1, '@')
        self.assertEqual(['a', '@', 'b', 'c'], list(self._list))
        self.assertEqual(['a', '@', 'b', 'c'], list(self._list2))

    def testInsertRight(self):
        self._list.insert(3, '@')
        self.assertEqual(['a', 'b', 'c', '@'], list(self._list))
        self.assertEqual(['a', 'b', 'c', '@'], list(self._list2))

    def testReplaceLeft(self):
        self._list[0] = '@'
        self.assertEqual(['@', 'b', 'c'], list(self._list))
        self.assertEqual(['@', 'b', 'c'], list(self._list2))

    def testReplaceMiddle(self):
        self._list[1] = '@'
        self.assertEqual(['a', '@', 'c'], list(self._list))
        self.assertEqual(['a', '@', 'c'], list(self._list2))

    def testReplaceRight(self):
        self._list[2] = '@'
        self.assertEqual(['a', 'b', '@'], list(self._list))
        self.assertEqual(['a', 'b', '@'], list(self._list2))

    def testDeleteLeft(self):
        del self._list[0]
        self.assertEqual(['b', 'c'], list(self._list))
        self.assertEqual(['b', 'c'], list(self._list2))

    def testDeleteMiddle(self):
        del self._list[1]
        self.assertEqual(['a', 'c'], list(self._list))
        self.assertEqual(['a', 'c'], list(self._list2))

    def testDeleteRight(self):
        del self._list[2]
        self.assertEqual(['a', 'b'], list(self._list))
        self.assertEqual(['a', 'b'], list(self._list2))

    def testDeleteContiguousSlice(self):
        del self._list[0:2]
        self.assertEqual(['c'], list(self._list))
        self.assertEqual(['c'], list(self._list2))

    def testDeleteNonContiguousSlice(self):
        del self._list[0:3:2]
        self.assertEqual(['b'], list(self._list))
        self.assertEqual(['b'], list(self._list2))

    def testSwapNonAdjacent(self):
        self._list.swap(0, 2)
        self.assertEqual(['c', 'b', 'a'], list(self._list))
        self.assertEqual(['c', 'b', 'a'], list(self._list2))

    def testSwapAdjacentHead(self):
        self._list.swap(0, 1)
        self.assertEqual(['b', 'a', 'c'], list(self._list))
        self.assertEqual(['b', 'a', 'c'], list(self._list2))

    def testSwapAdjacentTail(self):
        self._list.swap(1, 2)
        self.assertEqual(['a', 'c', 'b'], list(self._list))
        self.assertEqual(['a', 'c', 'b'], list(self._list2))

