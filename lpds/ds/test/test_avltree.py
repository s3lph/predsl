
import unittest

import random

from lpds.ds.avltree import AVLTreeRW
from lpds.log.memory import InMemoryAppendOnlyLog


class TestAVLTree(unittest.TestCase):

    def setUp(self) -> None:
        log = InMemoryAppendOnlyLog()
        log2 = InMemoryAppendOnlyLog(replicate=log)
        self._tree = AVLTreeRW(log)
        self._tree.add_node(42)
        self._tree2 = log2.get_datastructure(0)

    def buildTree(self):
        for i in range(100):
            if i != 42:
                self._tree.insert(i)

    def _testBalance(self, tree):
        # All nodes must be balanced at all times
        for node in tree:
            self.assertLess(node.balance, 2)
            self.assertGreater(node.balance, -2)

    def testSearch(self):
        self.buildTree()
        for i in range(100):
            n = self._tree.search(i)
            self.assertIsNotNone(n)
            self.assertEqual(i, n.value)
        for i in range(100):
            n = self._tree2.search(i)
            self.assertIsNotNone(n)
            self.assertEqual(i, n.value)
        self.assertIsNone(self._tree.search(-1))
        self.assertIsNone(self._tree.search(100))
        self.assertIsNone(self._tree2.search(-1))
        self.assertIsNone(self._tree2.search(100))

    def testBalance(self):
        self.buildTree()
        self._testBalance(self._tree)
        self._testBalance(self._tree2)
        self.assertEqual(7, self._tree._root.height)
        self.assertEqual(7, self._tree2._root.height)

    def testAddNodes(self):
        self.buildTree()
        values = random.sample([x+0.5 for x in range(-10, 109)], k=30)
        for v in values:
            self._tree.insert(v)
        self._testBalance(self._tree)
        self._testBalance(self._tree2)
        self.assertIn(self._tree._root.height, [8, 9])
        self.assertIn(self._tree2._root.height, [8, 9])
        for v in values:
            n = self._tree2.search(v)
            self.assertIsNotNone(n)
            self.assertEqual(v, n.value)

    def testRemoveNodes(self):
        self.buildTree()
        toremove = random.sample(range(100), k=70)
        tokeep = set(range(100)).difference(toremove)
        for v in toremove:
            self._tree.delete(v)
        self._testBalance(self._tree)
        self._testBalance(self._tree2)
        self.assertIn(self._tree._root.height, [5, 6])
        self.assertIn(self._tree2._root.height, [5, 6])
        for v in tokeep:
            n = self._tree.search(v)
            self.assertIsNotNone(n)
            self.assertEqual(v, n.value)
            n = self._tree2.search(v)
            self.assertIsNotNone(n)
            self.assertEqual(v, n.value)
        for v in toremove:
            n = self._tree.search(v)
            self.assertIsNone(n)
            n = self._tree2.search(v)
            self.assertIsNone(n)

