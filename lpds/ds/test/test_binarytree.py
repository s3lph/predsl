
import unittest

from lpds.ds.binarytree import BinaryTreeRO, BinaryTreeRW
from lpds.log.memory import InMemoryAppendOnlyLog


class TestBinaryTree(unittest.TestCase):

    def setUp(self) -> None:
        log = InMemoryAppendOnlyLog()
        log2 = InMemoryAppendOnlyLog(replicate=log)
        self._tree = BinaryTreeRW(log)
        self._tree.add_node(42)
        self._tree2 = log2.get_datastructure(0)

    def buildTree(self):
        root = self._tree.root
        self._tree.add_node(23, parent=root, ppos=0)
        self._tree.add_node(7, parent=root, ppos=1)
        n7 = root[1]
        self._tree.add_node(1337, parent=n7, ppos=0)
        self._tree.add_node(4, parent=n7, ppos=1)

    def testAddNodes(self):
        self.buildTree()
        values = [n.value for n in self._tree]
        values2 = [n.value for n in self._tree2]
        self.assertEqual([42, 23, 7, 1337, 4], values)
        self.assertEqual([42, 23, 7, 1337, 4], values2)

    def testRemoveNode(self):
        self.buildTree()
        self._tree.remove_node(self._tree.root[1])
        values = [n.value for n in self._tree]
        values2 = [n.value for n in self._tree2]
        self.assertEqual(2, len(self._tree.root.children))
        self.assertEqual(2, len(self._tree2.root.children))
        self.assertEqual([42, 23], values)
        self.assertEqual([42, 23], values2)

    def testClear(self):
        self.buildTree()
        self._tree.clear()
        self.assertEqual(0, len(self._tree))
        self.assertEqual(0, len(self._tree2))

    def testBalance(self):
        self.buildTree()
        self.assertEqual(1, self._tree.balance())
        self._tree.remove_node(self._tree.root[0])
        self.assertEqual(2, self._tree.balance())

    def _testRebalance(self, expected):
        values = [n.value for n in self._tree.inorder()]
        values2 = [n.value for n in self._tree2.inorder()]
        self.assertEqual(expected, values)
        self.assertEqual(expected, values2)
        self.assertEqual(21, self._tree.root.height)
        self._tree.rebalance()
        values3 = [n.value for n in self._tree.inorder()]
        values4 = [n.value for n in self._tree2.inorder()]
        self.assertEqual(expected, values3)
        self.assertEqual(expected, values4)
        self.assertEqual(5, self._tree.root.height)

    def testRebalanceRight(self):
        for i in range(20):
            rightmost = self._tree.root
            while rightmost.right is not None:
                rightmost = rightmost.right
            self._tree.add_node(i, parent=rightmost, ppos=1)
        self._testRebalance([42] + list(range(20)))

    def testRebalanceLeft(self):
        for i in range(20):
            leftmost = self._tree.root
            while leftmost.left is not None:
                leftmost = leftmost.left
            self._tree.add_node(i, parent=leftmost, ppos=0)
        self._testRebalance(list(range(19, -1, -1)) + [42])
