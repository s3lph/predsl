
import unittest

from lpds.ds.dictionary import DictionaryRW, DictionaryRO
from lpds.log.memory import InMemoryAppendOnlyLog


class TestDictionary(unittest.TestCase):

    def setUp(self) -> None:
        log = InMemoryAppendOnlyLog()
        log2 = InMemoryAppendOnlyLog(replicate=log)
        self._dict = DictionaryRW(log)
        self._dict['foo'] = 'a'
        self._dict['bar'] = 'b'
        self._dict2 = log2.get_datastructure(0)

    def testInsertNewKvPair(self):
        self._dict['baz'] = 'c'
        self.assertEqual({'foo': 'a', 'bar': 'b', 'baz': 'c'}, dict(self._dict))
        self.assertEqual({'foo': 'a', 'bar': 'b', 'baz': 'c'}, dict(self._dict2))

    def testReplaceValue(self):
        self._dict['bar'] = 'c'
        self.assertEqual({'foo': 'a', 'bar': 'c'}, dict(self._dict))
        self.assertEqual({'foo': 'a', 'bar': 'c'}, dict(self._dict2))

    def testDeleteValue(self):
        del self._dict['bar']
        self.assertEqual({'foo': 'a'}, dict(self._dict))
        self.assertEqual({'foo': 'a'}, dict(self._dict2))

    def testClear(self):
        self._dict.clear()
        self.assertEqual({}, dict(self._dict))
        self.assertEqual({}, dict(self._dict2))
