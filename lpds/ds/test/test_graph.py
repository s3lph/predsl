
import unittest

from lpds.ds.graph import GraphRO, GraphRW, DirectedWeightedGraphRW, DirectedWeightedGraphRO
from lpds.log.memory import InMemoryAppendOnlyLog


class TestSet(unittest.TestCase):

    def setUp(self) -> None:
        log = InMemoryAppendOnlyLog()
        log2 = InMemoryAppendOnlyLog(replicate=log)
        self._graph = GraphRW(log)
        self._graph.add(42)
        self._graph2 = log2.get_datastructure(0)
        self._dwgraph = DirectedWeightedGraphRW(log)
        self._dwgraph.add(42)
        self._dwgraph2 = log2.get_datastructure(1)

    def testGraphProperties(self):
        self.assertEqual(False, self._graph.directed)
        self.assertEqual(False, self._graph.weighted)
        self.assertEqual(False, self._graph2.directed)
        self.assertEqual(False, self._graph2.weighted)
        self.assertEqual(True, self._dwgraph.directed)
        self.assertEqual(True, self._dwgraph.weighted)
        self.assertEqual(True, self._dwgraph2.directed)
        self.assertEqual(True, self._dwgraph2.weighted)

    def testAddNodes(self):
        n1 = 0  # Workaround bc first node already added
        n2 = self._graph.add(23)
        n3 = self._graph.add(7)
        n4 = self._graph.add(1337)
        self._graph.link(n1, n2)
        self._graph.link(n2, n3)
        self._graph.link(n2, n2)
        self._graph.link(n3, n4)
        self._graph.link(n4, n1)
        self._graph.link(n1, n3)
        dn1 = 1
        dn2 = self._dwgraph.add(23)
        dn3 = self._dwgraph.add(7)
        dn4 = self._dwgraph.add(1337)
        self._dwgraph.link(dn1, dn2, 1)
        self._dwgraph.link(dn2, dn3, 2)
        self._dwgraph.link(dn2, dn2, 0)
        self._dwgraph.link(dn3, dn4, 3)
        self._dwgraph.link(dn4, dn1, 4)
        self._dwgraph.link(dn1, dn3, 5)

    def testClear(self):
        self._graph.add(23)
        self.assertEqual(2, len(self._graph))
        self.assertEqual(2, len(self._graph2))
        self._graph.clear()
        self.assertEqual(0, len(self._graph))
        self.assertEqual(0, len(self._graph2))

    def testRemove(self):
        self._graph.add(23)
        self._graph.remove(0)  # node inserted in setup
        self.assertEqual(1, len(self._graph))
        self.assertEqual(1, len(self._graph2))
        self.assertIn(23, self._graph)
        self.assertIn(23, self._graph2)

    def testLink(self):
        n1 = 0
        n2 = self._graph.add(23)
        self._graph.link(n1, n1)
        self._graph.link(n1, n2)
        self.assertIn((n1, n1), self._graph)
        self.assertIn((n1, n2), self._graph)
        self.assertIn((n2, n1), self._graph)
        self.assertNotIn((n2, n2), self._graph)
        dn1 = 1
        dn2 = self._dwgraph.add(7)
        self._dwgraph.link(dn1, dn1, 42)
        self._dwgraph.link(dn1, dn2, 42)
        self.assertIn((dn1, dn1), self._dwgraph)
        self.assertIn((dn1, dn2), self._dwgraph)
        self.assertNotIn((dn2, dn1), self._dwgraph)
        self.assertNotIn((dn2, dn2), self._dwgraph)

    def testUnlink(self):
        n1 = 0
        n2 = self._graph.add(23)
        n3 = self._graph.add(7)
        self._graph.link(n1, n1)
        self._graph.link(n1, n2)
        self._graph.link(n2, n3)
        self._graph.link(n3, n1)
        self._graph.unlink(n3, n2)
        self.assertIn((n1, n1), self._graph)
        self.assertIn((n1, n2), self._graph)
        self.assertIn((n2, n1), self._graph)
        self.assertIn((n1, n3), self._graph)
        self.assertIn((n3, n1), self._graph)
        self.assertNotIn((n2, n3), self._graph)
        self.assertNotIn((n3, n2), self._graph)
        self._graph.unlink(n1, n1)
        self.assertNotIn((n1, n1), self._graph)

