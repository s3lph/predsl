
import unittest

from lpds.ds.set import SetRW, SetRO
from lpds.log.memory import InMemoryAppendOnlyLog


class TestSet(unittest.TestCase):

    def setUp(self) -> None:
        log = InMemoryAppendOnlyLog()
        log2 = InMemoryAppendOnlyLog(replicate=log)
        self._set = SetRW(log)
        self._set.add(0)
        self._set.add(1)
        self._set.add(2)
        self._set.add(3)
        self._other = SetRW(log)
        self._other.add(0)
        self._other.add(2)
        self._other.add(4)
        self._other.add(6)
        self._set2 = log2.get_datastructure(0)

    def testAddNewKey(self):
        self._set.add(42)
        self.assertEqual({0, 1, 2, 3, 42}, set(self._set))
        self.assertEqual({0, 1, 2, 3, 42}, set(self._set2))

    def testDeleteKey(self):
        self._set.remove(2)
        self.assertEqual({0, 1, 3}, set(self._set))
        self.assertEqual({0, 1, 3}, set(self._set2))

    def testClear(self):
        self._set.clear()
        self.assertEqual({}, dict(self._set))
        self.assertEqual({}, dict(self._set2))

    def testUpdateIntersect(self):
        self._set.intersection_update(self._other)
        self.assertEqual({0, 2}, set(self._set))
        self.assertEqual({0, 2}, set(self._set2))

    def testUpdateUnion(self):
        self._set.update(self._other)
        self.assertEqual({0, 1, 2, 3, 4, 6}, set(self._set))
        self.assertEqual({0, 1, 2, 3, 4, 6}, set(self._set2))

    def testUpdateDifference(self):
        self._set.difference_update(self._other)
        self.assertEqual({1, 3}, set(self._set))
        self.assertEqual({1, 3}, set(self._set2))

    def testUpdateSymmetricDifference(self):
        self._set.symmetric_difference_update(self._other)
        self.assertEqual({1, 3, 4, 6}, set(self._set))
        self.assertEqual({1, 3, 4, 6}, set(self._set2))
