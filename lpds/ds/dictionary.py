from dataclasses import dataclass
from typing import Optional

from lpds.ds.ds import Datastructure


class DictionaryRO(Datastructure):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None, subscribe=True):
        super().__init__(log, duid, subscribe)
        self._log = log
        self.duid = duid
        if subscribe:
            log.subscribe(self, duid)
        self._dict = {}
        self.consumed = 0

    def __iter__(self):
        return self._dict.__iter__()

    def consume_entry(self, entry, unsafe: bool = False):
        if entry['duid'] != self.duid:
            return
        self.consumed += 1
        myid = entry.get('serial')
        key = entry.get('key')
        if entry.get('delete', False):
            if key is None:
                # delete without key -> clear the dict
                self._dict.clear()
            elif key in self._dict:
                # delete with key -> clear the key
                del self._dict[key]
        else:
            # key and value -> set key to value
            value = entry.get('value')
            if value['type'] == 'immediate':
                value = value['value']
            else:
                value = self._log.get_datastructure(value['duid'])
            self._dict[key] = value

    def get(self, k, default=None):
        return self._dict.get(k, default)

    def __getitem__(self, k):
        return self._dict.__getitem__(k)

    def __len__(self):
        return len(self._dict)

    def __dict__(self):
        return self._dict

    def keys(self):
        return self._dict.keys()

    def values(self):
        return self._dict.values()

    def items(self):
        return self._dict.items()

    def ref_dict(self):
        return {
            'type': 'dictionary',
            'duid': self.duid
        }


class DictionaryRW(DictionaryRO):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None):
        if duid is None:
            duid = log.gen_duid(self)
        super().__init__(log, duid, False)
        # self._oldest = len(self._log)
        self._key_serials = {}

    def rewrite(self, serial: int, entry: dict = None) -> dict:
        keys = [k for k, v in self._key_serials.items() if v == serial]
        if len(keys) == 0:
            return None
        key = keys[0]
        myid = len(self._log)
        if entry is not None:
            if entry['duid'] != self.duid:
                raise RuntimeError('Wrong DUID')
            elif 'value' in entry:
                raise RuntimeError('Entry already has a value')
        else:
            entry = self.ref_dict()
            entry['serial'] = myid
        entry['key'] = key
        entry['value'] = self.wrap(self._dict[key])
        self._key_serials[key] = myid
        return entry

    def __setitem__(self, key, value, /):
        myid = len(self._log)
        entry = self.ref_dict()
        entry.update({
            'key': key,
            'value': self.wrap(value),
            'serial': myid
        })
        self._key_serials[key] = myid
        self._log.append(entry)

    def clear(self):
        myid = len(self._log)
        entry = self.ref_dict()
        entry.update({
            'serial': myid,
            'delete': True,
            '_do_not_rewrite': True,
        })
        self._key_serials.clear()
        self._log.append(entry)

    def __delitem__(self, key, /):
        myid = len(self._log)
        entry = self.ref_dict()
        entry.update({
            'serial': myid,
            'key': key,
            'delete': True,
            '_do_not_rewrite': True,
        })
        del self._key_serials[key]
        self._log.append(entry)

    def oldest(self, *, older_than: int = -1):
        return min((x for x in self._key_serials.values() if x > older_than), default=0)
