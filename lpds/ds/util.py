import json
import struct

from lpds.ds.linkedlist import LinkedListRO
from lpds.ds.dictionary import DictionaryRO
from lpds.ds.set import SetRO


# TODO: Rethink chain20 approach (why write a P48 packet AND a C20
#  sidechain, when the C20 sidechain is created anyways)

def chain20_write(buffer, log):
    return 0  # TODO: Write to chain20 and return sequence number


def chain20_read(seq, log):
    return b''  # TODO: Read from chain20 and return content


def p48_encode_value(value, log):
    if value is None:
        ctype = 1
        create_encoded = b'\x00' * 8
    elif isinstance(value, bool):
        ctype = 2
        create_encoded = struct.pack('!L', int(value))
    elif isinstance(value, int):
        ctype = 3
        create_encoded = struct.pack('!l', value)
    elif isinstance(value, float):
        ctype = 4
        create_encoded = struct.pack('!d', value)
    elif isinstance(value, str) or isinstance(value, bytes):
        if isinstance(value, str):
            value = value.encode('utf-8')
        chain20s = chain20_write(value, log)
        ctype = 5
        create_encoded = struct.pack('!II', chain20s, len(value))
    elif isinstance(value, LinkedListRO):
        ctype = 6
        create_encoded = struct.pack('!4xI', value.duid)
    elif isinstance(value, DictionaryRO):
        ctype = 7
        create_encoded = struct.pack('!4xI', value.duid)
    else:
        raise NotImplementedError(f'Cannot encode object of type {type(value)}')
    return ctype, create_encoded


def p48_decode_value(ctype, encoded, log):
    if ctype == 0:
        raise ValueError('Attempted to decode value indicating absence of a value')
    elif ctype == 1:
        return None
    elif ctype == 2:
        return bool(struct.unpack('!L', encoded))
    elif ctype == 3:
        return struct.unpack('!l', encoded)
    elif ctype == 4:
        return struct.unpack('!d', encoded)
    elif ctype == 5:
        chain20s, length = struct.unpack('!II', encoded)
        return chain20_read(chain20s, log)
    elif ctype == 6:
        cduid = struct.unpack('!4xI', encoded)
        return from_duid(log, LinkedListRO, cduid)
    elif ctype == 7:
        cduid = struct.unpack('!4xI', encoded)
        return from_duid(log, DictionaryRO, cduid)
    else:
        raise NotImplementedError(f'Unknown type {ctype}')


def linkedlist_entry_to_p48(linkedlist: LinkedListRO, entry, log):
    serial = entry['serial']
    dtype = 0x06
    nlinks = len(entry.get('links', []))
    link1f = entry['links'][0][0] if nlinks > 0 else 0
    link1t = entry['links'][0][1] if nlinks > 0 else 0
    link2f = entry['links'][1][0] if nlinks > 1 else 0
    link2t = entry['links'][1][1] if nlinks > 1 else 0
    head = entry.get('head', 0)
    head_set = 1 if 'head' in entry else 0
    oldest = entry.get('oldest', 0)
    oldest_set = 1 if 'oldest' in entry else 0
    flags = 0
    flags |= (nlinks & 0x03) << 6
    flags |= head_set << 5
    flags |= oldest_set << 4

    if 'create' in entry:
        if nlinks > 2:
            raise ValueError('Must only contain up to two links')
        flags |= 1 << 3
        ctype, create_encoded = p48_encode_value(entry['create'], log)

        return struct.pack('!IIBBB II II 8s I I 5x',
                           serial, linkedlist.duid, dtype, flags, ctype,
                           link1f, link1t, link2f, link2t, create_encoded,
                           head, oldest
                           )
    else:
        if nlinks > 3:
            raise ValueError('Must only contain up to three links')
        link3f = entry['links'][2][0] if nlinks > 2 else 0
        link3t = entry['links'][2][1] if nlinks > 2 else 0
        ctype = 0
        return struct.pack('!IIBBB II II II I I 5x',
                           serial, linkedlist.duid, dtype, flags, ctype,
                           link1f, link1t, link2f, link2t, link3f, link3t,
                           head, oldest)


def linkedlist_entry_from_p48(log, p48: bytes):
    serial, duid, dtype, flags, ctype = struct.unpack_from('!IIB', p48)
    nlinks = (flags & 0xc0) >> 6
    head_set = (flags & 0x20) > 0
    oldest_set = (flags & 0x10) > 0
    entry = {
        'serial': serial,
    }

    if ctype == 0:
        link1f, link1t, link2f, link2t, link3f, link3t, head, oldest = \
            struct.unpack_from('!II II II I I 5x', p48, offset=11)
    else:
        link1f, link1t, link2f, link2t, create, head, oldest = \
            struct.unpack_from('!II II 8s I I 5x', p48, offset=11)
        link3f, link3t = None, None
        entry['create'] = p48_decode_value(ctype, create, log)

    if head_set:
        entry['head'] = head
    if oldest_set:
        entry['oldest'] = oldest
    if nlinks == 1:
        entry['links'] = [[link1f, link1t]]
    elif nlinks == 2:
        entry['links'] = [[link1f, link1t], [link2f, link2t]]
    elif nlinks == 3 and ctype != 0:
        entry['links'] = [[link1f, link1t], [link2f, link2t], [link3f, link3t]]
    return entry


def dict_entry_to_p48(dictionary: LinkedListRO, entry, log):
    serial = entry['serial']
    dtype = 0x07
    oldest = entry.get('oldest', 0)
    oldest_set = 1 if 'oldest' in entry else 0
    key_immediate = 'key' in entry and len(entry['key']) <= 16
    clear = entry.get('delete', False) and 'key' not in entry
    flags = 0
    flags |= oldest_set << 7
    flags |= key_immediate << 5
    flags |= clear << 4

    if clear:
        return struct.pack('!IIBBB I 29x',
                           serial, dictionary.duid, dtype, flags, 0, oldest)

    key = entry['key'].encode('utf-8')
    keylen = len(key)
    if key_immediate:
        keyenc = struct.pack(f'!{keylen}s {16-keylen}x', key)
    else:
        chain20s = chain20_write(key, log)
        keyenc = struct.pack('! I 12x', chain20s)

    if not entry.get('delete', False):
        ctype, create_encoded = p48_encode_value(entry['value'], log)
    else:
        ctype = 0
        create_encoded = b'\x00' * 8

    return struct.pack('!IIBBB I I 16s 8s 5x',
                       serial, dictionary.duid, dtype, flags, ctype,
                       oldest, keylen, keyenc, create_encoded)


def dict_entry_from_p48(log, p48: bytes):
    serial, duid, dtype, flags, ctype, oldest, keylen, keyenc, create = \
        struct.unpack_from('!IIBBB I I 16s 8s 5x', p48)
    oldest_set = (flags & 0x80) > 0
    key_immediate = (flags & 0x40) > 0
    clear = (flags & 0x20) > 0
    entry = {
        'serial': serial,
    }
    if oldest_set:
        entry['oldest'] = oldest
    if clear:
        entry['delete'] = True
        return entry

    if key_immediate:
        entry['key'] = keyenc[:keylen].decode()
    else:
        chain20s = struct.unpack_from('!I 12x', keyenc)
        entry['key'] = chain20_read(chain20s, log)

    if ctype == 0:
        entry['delete'] = True
    else:
        entry['value'] = p48_decode_value(ctype, create, log)

    return entry


DUID_MAP = {}


def from_duid(log, cls, duid):
    if duid in DUID_MAP:
        return DUID_MAP[duid]
    instance = cls(log, duid, subscribe=True)
    DUID_MAP[duid] = instance
    return instance


def entry_from_p48(log, p48: bytes):
    serial, duid, dtype = struct.unpack_from('!IIB', p48)
    if dtype == 6:
        return linkedlist_entry_from_p48(log, p48)
    elif dtype == 7:
        return dict_entry_from_p48(log, p48)
    else:
        raise NotImplementedError(f'Unknown data structure with id {dtype}')


class LpdsJsonEncoder(json.JSONEncoder):

    def default(self, o):
        if isinstance(o, LinkedListRO) or isinstance(o, SetRO):
            return list(o)
        elif isinstance(o, DictionaryRO):
            return dict(o)
            # return {k: self.encode(v) for k, v in o.items()}
        return super().default(o)
