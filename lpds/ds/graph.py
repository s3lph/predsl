
from graphviz import Graph, Digraph

from lpds.ds.ds import Datastructure


class GraphRO(Datastructure):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None, subscribe=True):
        super().__init__(log, duid, subscribe)
        self._log = log
        self.duid = duid
        if subscribe:
            log.subscribe(self, duid)
        self._adjacency = {}
        self._nodes = {}
        self.consumed = 0
        self._directed = False
        self._weighted = False

    def consume_entry(self, entry, unsafe: bool = False):
        if entry['duid'] != self.duid:
            return
        self.consumed += 1
        serial = entry['serial']
        if 'value' in entry:
            value = entry.get('value')
            if value['type'] == 'immediate':
                value = value['value']
            else:
                value = self._log.get_datastructure(value['duid'])
            self._nodes[serial] = value
        if 'delete' in entry:
            for todel in entry['delete']:
                if todel == -1:
                    self._nodes.clear()
                    self._adjacency.clear()
                    break
                else:
                    self._nodes.pop(todel, None)
                    self._adjacency.pop(todel, None)
                    for incoming in self._adjacency.values():
                        incoming.pop(todel, None)
        unlink = entry.get('unlink', [])
        for efrom, eto in unlink:
            odict = self._adjacency.get(efrom, {})
            odict.pop(eto, None)
            if not self._directed:
                idict = self._adjacency.get(eto, {})
                idict.pop(efrom, None)
        edges = entry.get('edges', [])
        for edge in edges:
            if self._weighted:
                efrom, eto, eweight = edge
            else:
                efrom, eto = edge
                eweight = 1
            self._adjacency.setdefault(efrom, {})[eto] = eweight
            if not self._directed:
                self._adjacency.setdefault(eto, {})[efrom] = eweight

    def __len__(self):
        return len(self._nodes)

    @property
    def directed(self):
        return self._directed

    @property
    def weighted(self):
        return self._weighted

    @property
    def nodes(self):
        return list(self._nodes.keys())

    @property
    def edges(self):
        edges = []
        for efrom, e in self._adjacency.items():
            for eto, weight in e.items():
                if self._weighted:
                    if (efrom, eto, weight) not in edges and (self.directed or (eto, efrom, weight) not in edges):
                        edges.append((efrom, eto, weight))
                else:
                    if (efrom, eto) not in edges and (self.directed or (eto, efrom) not in edges):
                        edges.append((efrom, eto))
        return edges

    def __contains__(self, item):
        if isinstance(item, tuple):
            f, t = item
            return self._adjacency.get(f, {}).get(t) is not None
        else:
            return item in self._nodes.values()

    def __getitem__(self, item):
        return self._nodes[item]

    def __eq__(self, other):
        if not isinstance(other, GraphRO):
            return False
        if self.weighted != other.weighted or self.directed != other.directed:
            return False
        if self._nodes != other._nodes:
            return False
        for n in self.nodes:
            if self._adjacency.get(n, {}) != other._adjacency.get(n, {}):
                return False
        return True

    def ref_dict(self):
        return {
            'type': 'graph',
            'duid': self.duid
        }

    def graphviz(self):
        if self.directed:
            dot = Digraph()
        else:
            dot = Graph()
        for k, v in self._nodes.items():
            dot.node(str(k), str(v))
        handled = set()
        for n1, e in self._adjacency.items():
            for n2, v in e.items():
                if (n1, n2) in handled or (not self.directed and (n2, n1) in handled):
                    continue
                if self.weighted:
                    dot.edge(str(n1), str(n2), label=str(v))
                else:
                    dot.edge(str(n1), str(n2))
                handled.add((n1, n2))
        return dot


class GraphRW(GraphRO):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None):
        if duid is None:
            duid = log.gen_duid(self)
        super().__init__(log, duid, False)

    def rewrite(self, serial: int, entry: dict = None) -> dict:
        if serial not in self._nodes:
            return
        myid = len(self._log)
        if entry is not None:
            for e in entry.get('edges', []):
                if e[0] == serial or e[1] == serial:
                    return entry
            edges = entry.get('edges', [])
        else:
            edges = []
        for eto, eweight in self._adjacency.get(serial, {}).items():
            if self._weighted:
                edges.append([myid, eto, eweight])
            else:
                edges.append([myid, eto])
        for efrom, eto in self._adjacency.items():
            if efrom == serial:
                continue
            if serial in eto:
                if self._weighted:
                    edges.append([efrom, myid, eto[serial]])
                else:
                    edges.append([efrom, myid])
        if entry is None:
            entry = self.ref_dict()
            entry['serial'] = myid
        entry.update({
            'value': self.wrap(self._nodes[serial]),
            'delete': [serial],
            'edges': edges
        })
        return entry

    def add(self, node, /, edges=None):
        myid = len(self._log)
        entry = self.ref_dict()
        entry.update({
            'value': self.wrap(node),
            'serial': myid
        })
        if edges is not None and len(edges) > 0:
            entry['edges'] = edges
        self._log.append(entry)
        return myid

    def link(self, efrom, eto, eweight=1):
        myid = len(self._log)
        if self._weighted:
            edge = [efrom, eto, eweight]
        else:
            edge = [efrom, eto]
        edges = [edge]
        entry = self.ref_dict()
        entry.update({
            'serial': myid,
            'edges': edges
        })
        self._log.append(entry)

    def unlink(self, efrom, eto):
        myid = len(self._log)
        entry = self.ref_dict()
        entry.update({
            'serial': myid,
            'unlink': [[efrom, eto]]
        })
        self._log.append(entry)

    def clear(self):
        myid = len(self._log)
        entry = self.ref_dict()
        entry.update({
            'serial': myid,
            'delete': [-1]
        })
        self._log.append(entry)

    def remove(self, node, /):
        myid = len(self._log)
        entry = self.ref_dict()
        entry.update({
            'serial': myid,
            'delete': [node],
            '_do_not_rewrite': True
        })
        self._log.append(entry)

    def oldest(self, *, older_than: int = -1) -> int:
        return min((x for x in self._nodes.keys() if x > older_than), default=older_than)


class DirectedGraphRO(GraphRO):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None, subscribe=True):
        super().__init__(log, duid, subscribe)
        self._directed = True

    def ref_dict(self):
        return {
            'type': 'dgraph',
            'duid': self.duid
        }


class DirectedGraphRW(GraphRW):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None):
        super().__init__(log, duid)
        self._directed = True

    def ref_dict(self):
        return {
            'type': 'dgraph',
            'duid': self.duid
        }


class DirectedWeightedGraphRO(GraphRO):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None, subscribe=True):
        super().__init__(log, duid, subscribe)
        self._directed = True
        self._weighted = True

    def ref_dict(self):
        return {
            'type': 'dwgraph',
            'duid': self.duid
        }


class DirectedWeightedGraphRW(GraphRW):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None):
        super().__init__(log, duid)
        self._directed = True
        self._weighted = True

    def ref_dict(self):
        return {
            'type': 'dwgraph',
            'duid': self.duid
        }


class WeightedGraphRO(GraphRO):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None, subscribe=True):
        super().__init__(log, duid, subscribe)
        self._weighted = True

    def ref_dict(self):
        return {
            'type': 'wgraph',
            'duid': self.duid
        }


class WeightedGraphRW(GraphRW):

    def __init__(self, log: 'lpds.log.log.AppendOnlyLog', duid: int = None):
        super().__init__(log, duid)
        self._weighted = True

    def ref_dict(self):
        return {
            'type': 'wgraph',
            'duid': self.duid
        }
