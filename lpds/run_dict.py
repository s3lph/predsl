import string
from random import randint, choice

from lpds.ds.dictionary import DictionaryRO, DictionaryRW
from lpds.log.memory import InMemoryAppendOnlyLog

from matplotlib import pyplot as plot
from tqdm import tqdm


if __name__ == '__main__':

    log = InMemoryAppendOnlyLog()
    log1 = InMemoryAppendOnlyLog(replicate=log)

    ds0 = DictionaryRW(log)

    for _ in tqdm(range(10000)):
        if randint(0, 2) == 0 and len(ds0) > 0:
            k = choice(list(ds0.keys()))
            del ds0[k]
        else:
            k = randint(0, 1000)
            v = choice(string.ascii_letters)
            ds0[k] = v

    log2 = InMemoryAppendOnlyLog(replicate=log)

    for _ in tqdm(range(10000)):
        if randint(0, 2) == 0 and len(ds0) > 0:
            k = choice(list(ds0.keys()))
            del ds0[k]
        else:
            k = randint(0, 1000)
            v = choice(string.ascii_letters)
            ds0[k] = v

    log3 = InMemoryAppendOnlyLog(replicate=log)

    ds1 = log1.get_datastructure(0)
    ds2 = log2.get_datastructure(0)
    ds3 = log3.get_datastructure(0)

    print(ds0.oldest())

    print(len(ds0))
    print(min(ds0._key_serials.values()) if len(ds0._key_serials.values()) > 0 else None)
    print(dict(ds0) == dict(ds1), ds1.consumed)
    print(dict(ds0) == dict(ds2), ds2.consumed)
    print(dict(ds0) == dict(ds3), ds3.consumed)

    # plot.hist(ds0._key_serials.values(), bins=25, range=(0, len(log)))
    # plot.axvline(log.oldest(), color='red')
    xs = []
    ys = []
    cs = []
    s = []
    for i in range(log.oldest()):
        xs.append(i)
        ys.append(i % 150)
        s.append(4)
        cs.append('red')
    for i in log.sliding_window:
        if i not in ds0._key_serials.values():
            xs.append(i)
            ys.append(i % 150)
            s.append(4)
            cs.append('yellow')
    for i in log.sliding_window:
        if i in ds0._key_serials.values():
            xs.append(i)
            ys.append(i % 150)
            s.append(9)
            cs.append('blue')
    plot.scatter(xs, ys, s=s, c=cs)
    plot.show()
