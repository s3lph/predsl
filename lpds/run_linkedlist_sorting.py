import string
from random import randint, choice

from lpds.ds.linkedlist import LinkedListRO, LinkedListRW
from lpds.log.memory import InMemoryAppendOnlyLog

from matplotlib import pyplot as plot
from tqdm import tqdm


log = InMemoryAppendOnlyLog()
ds0 = LinkedListRW(log)

log1 = InMemoryAppendOnlyLog(replicate=log)

for _ in tqdm(range(10000)):
    ds0.insert(0, randint(0, 5000))

log2 = InMemoryAppendOnlyLog(replicate=log)

ds0.sort()
print(ds0)

log3 = InMemoryAppendOnlyLog(replicate=log)

for i in range(10):
    for _ in tqdm(range(1000)):
        ds0.insert(0, randint(0, 5000))
    ds0.sort()


ds1 = log1.get_datastructure(0)
ds2 = log1.get_datastructure(0)
ds3 = log1.get_datastructure(0)

print(len(ds0))
print(min(ds0._items) if len(ds0._items) > 0 else None)
print(list(ds0) == list(ds1), ds1.consumed)
print(list(ds1) == list(ds2), ds2.consumed)
print(list(ds2) == list(ds3), ds3.consumed)
print(list(ds1) == sorted(list(ds1)))
print(list(ds3) == sorted(list(ds3)))
print(log.stats[-1][2])  # performance

plot.plot(log.stats)
plot.hlines([1, 1.9], colors=['forestgreen', 'red'], xmin=0, xmax=len(log))
plot.ylim(bottom=0)
plot.show()

plot.plot([x[0]/x[1] if x[1] > 0 else 1 for x in log.stats])
plot.plot([x[2] for x in log.stats], color='purple')
plot.hlines([1, 1.9], colors=['forestgreen', 'red'], xmin=0, xmax=len(log))
plot.ylim(bottom=0, top=4)
plot.show()

plot.hist(ds0._items.keys(), bins=25, range=(0, len(log)))
plot.axvline(log.oldest(), color='red')
plot.show()
