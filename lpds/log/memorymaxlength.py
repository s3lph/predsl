
from lpds.log.memory import InMemoryAppendOnlyLog


class InMemoryMaxLengthAppendOnlyLog(InMemoryAppendOnlyLog):

    def __init__(self, *args, **kwargs):
        self._max_length = kwargs.get('max_length', 25000)
        super().__init__(*args, **kwargs)

    def _append(self, entry, foo=0):
        if len(self.sliding_window) >= self._max_length:
            raise IndexError(f'Log entry exceeds max length of {self._max_length} (sliding window: {self.sliding_window})')
        serial = self._serial
        self._serial += 1
        self._log.append(entry)
        self._consume(entry)
        self.stats.append((len(self.sliding_window), self._max_length, self.performance, sum(len(x) for x in self._datastructures.values()), foo))
        return serial

    def _replicate(self, replicate: InMemoryAppendOnlyLog):
        if isinstance(replicate, InMemoryMaxLengthAppendOnlyLog):
            self._max_length = replicate._max_length
        else:
            self._max_length = float('inf')
        return super()._replicate(replicate)

    def _append_strategy_batch_rewrite(self, entry, kf, kt, prune=True):
        swbefore = self.sliding_window
        lbefore = sum([len(x) for x in self._datastructures.values()])
        oldest = min([x.oldest() for x in self._datastructures.values()])
        if oldest > self._oldest:
            entry['oldest'] = oldest
            self._oldest = oldest
        self._append(entry, foo=1)
        oldest = min([x.oldest() for x in self._datastructures.values()])
        lbeforerewrite = sum([len(x) for x in self._datastructures.values()])
        # Log pruning: if the log sliding window is k times longer than the data structure, rewrite the oldest value
        reallen = sum([len(x) for x in self._datastructures.values()])
        if prune and len(self) > 10 and len(self.sliding_window) > kf * self._max_length and reallen > 0:
            ret = {}
            k_before = len(self.sliding_window) / self._max_length
            print('start')
            while self.redundancy > 1.1 and ret is not None:
                duid = self[oldest]['duid']
                ds = self._datastructures[duid]
                ret = ds.rewrite(oldest)
                oldest = min([x.oldest(older_than=oldest) for x in self._datastructures.values()])
                if ret is not None:
                    if oldest > self._oldest:
                        ret['oldest'] = oldest
                    self._append(ret)
                    if oldest > self._oldest:
                        self._oldest = oldest
                    self._prune()
            if ret is None or len(ret) > 0:
                # Only true if at least one entry was pruned
                self._prune()
                k_after = len(self.sliding_window) / self._max_length
                lafter = sum([len(x) for x in self._datastructures.values()])
                print(f'k: {k_before} -> {k_after}')
                print(f'sw: {swbefore} -> {self.sliding_window}')
                print(f'len: {lbefore} -> {lafter}')
                if lbeforerewrite != lafter:
                    breakpoint()

    def append(self, entry: dict):
        if not self._writable:
            raise NotImplementedError('Read-only log instance')
        if '_do_not_rewrite' in entry:
            del entry['_do_not_rewrite']
        if len(self.sliding_window) < 0.9 * self._max_length:
            self.rcount += 1
        lbefore = sum([len(x) for x in self._datastructures.values()])
        self._append_strategy_batch_rewrite(entry, kf=0.9, kt=0.5)
        lafter = sum([len(x) for x in self._datastructures.values()])
        if abs(lafter - lbefore) > 10:
            print(f'Warning: size changed from {lbefore} to {lafter}')
            breakpoint()
