import abc
import json

from lpds import ds


class LogEntry(abc.ABC):

    def __init__(self):
        pass

    def __dict__(self):
        pass


DS_TYPES = {
    'linkedlist': ds.LinkedListRO,
    'dictionary': ds.DictionaryRO,
    'set': ds.SetRO,
    'graph': ds.GraphRO,
    'dgraph': ds.DirectedGraphRO,
    'wgraph': ds.WeightedGraphRO,
    'dwgraph': ds.DirectedWeightedGraphRO,
    'tree': ds.TreeRO,
    '2tree': ds.BinaryTreeRO,
    'avltree': ds.AVLTreeRO,
}


class AppendOnlyLog(abc.ABC):

    def __init__(self,
                 replicate: 'AppendOnlyLog' = None,
                 strategy: str = 'BP',
                 kf: float = 1.9, kt: float = 1.5):
        self._duid_gen = 0
        self._datastructures = {}
        self._subscribers = []
        self._oldest = 0
        self._writable = replicate is None
        if replicate is not None:
            self._replicate(replicate)
            replicate.subscribe(self, None)
        self.stats = []
        self.jstats = []
        self.rcount = 0

        self.strategy = strategy
        self.kf = kf
        self.kt = kt

    def __len__(self) -> int:
        pass

    def __getitem__(self, index: int) -> object:
        pass

    def __iter__(self) -> object:
        pass

    def oldest(self) -> int:
        pass

    @property
    def sliding_window(self) -> range:
        return range(self.oldest(), len(self))

    def gen_duid(self, item) -> int:
        duid = self._duid_gen
        self._duid_gen += 1
        self._datastructures[duid] = item
        return duid

    def append(self, entry: dict):
        if not self._writable:
            raise NotImplementedError('Read-only log instance')
        if 'P' in self.strategy:
            if 'value' not in entry and entry['duid'] in self._datastructures and '_do_not_rewrite' not in entry:
                ds = self._datastructures[entry['duid']]
                if len(ds) > 0:
                    ds.rewrite(ds.oldest(), entry)
                    self._prune()
        if '_do_not_rewrite' in entry:
            del entry['_do_not_rewrite']
        if self.redundancy <= self.kf:
            self.rcount += 1
        if '0' in self.strategy:
            self._append_strategy_e0(entry)
        elif 'L' in self.strategy:
            self._append_strategy_baseline(entry)
        elif 'S' in self.strategy:
            self._append_strategy_always_rewrite(entry, k=self.kf)
        elif 'B' in self.strategy:
            self._append_strategy_batch_rewrite(entry, kf=self.kf, kt=self.kt)
        else:
            raise AttributeError(f'Unknown strategy: {self.strategy}')

    def _append_strategy_e0(self, entry):
        self._append(entry)

    def _append_strategy_baseline(self, entry):
        oldest = min([x.oldest() for x in self._datastructures.values()])
        if oldest > self._oldest:
            entry['oldest'] = oldest
        self._append(entry)
        oldest = min([x.oldest() for x in self._datastructures.values()])
        if oldest > self._oldest:
            self._oldest = oldest
            self._prune()

    def _append_strategy_always_rewrite(self, entry, k, prune=True):
        oldest = min([x.oldest() for x in self._datastructures.values()])
        if oldest > self._oldest:
            entry['oldest'] = oldest
            self._oldest = oldest
            self._prune()
        self._append(entry)
        oldest = min([x.oldest() for x in self._datastructures.values()])
        # Log pruning: if the log sliding window is k times longer than the data structure, rewrite the oldest value
        serial = len(self)
        reallen = sum([len(x) for x in self._datastructures.values()])
        if prune and serial > 10 and reallen > 0:
            k_before = (serial - oldest) / reallen
            if serial - oldest > k * reallen:
                duid = self[oldest]['duid']
                ds = self._datastructures[duid]
                ret = ds.rewrite(oldest)
                if ret is not None:
                    self._append_strategy_always_rewrite(ret, k, prune=False)
                    self._prune()
                    k_after = (len(self) - self._oldest) / reallen
                    print(f'k: {k_before} -> {k_after}')

    def _append_strategy_batch_rewrite(self, entry, kf, kt, prune=True):
        oldest = min([x.oldest() for x in self._datastructures.values()])
        if oldest > self._oldest:
            entry['oldest'] = oldest
            self._oldest = oldest
            self._prune()
        self._append(entry)
        oldest = min([x.oldest() for x in self._datastructures.values()])
        # Log pruning: if the log sliding window is k times longer than the data structure, rewrite the oldest value
        reallen = sum([len(x) for x in self._datastructures.values()])
        if prune and len(self) > 10 and len(self) - oldest > kf * reallen and reallen > 0:
            ret = {}
            k_before = (len(self) - oldest) / reallen
            print('start')
            while len(self) - oldest > kt * reallen and ret is not None:
                duid = self[oldest]['duid']
                ds = self._datastructures[duid]
                ret = ds.rewrite(oldest)
                if ret is not None:
                    oldest = min([x.oldest(older_than=oldest) for x in self._datastructures.values()])
                    if oldest > self._oldest:
                        ret['oldest'] = oldest
                    self._append(ret)
                    if oldest > self._oldest:
                        self._oldest = oldest
                        self._prune()
            if ret is None or len(ret) > 0:
                # Only true if at least one entry was pruned
                self._prune()
                k_after = (len(self) - self._oldest) / reallen
                print(f'k: {k_before} -> {k_after}')

    def _append(self, entry: dict):
        swlen = len(self.sliding_window)
        reallen = sum([len(x) for x in self._datastructures.values()])
        self.stats.append((swlen, reallen, self.performance))

    def _replicate(self, replicate: 'AppendOnlyLog'):
        pass

    def _consume(self, entry, unsafe: bool = False):
        duid = entry['duid']
        if duid not in self._datastructures:
            cls = DS_TYPES[entry['type']]
            instance = cls(log=self, duid=duid, subscribe=False)
            self._datastructures[duid] = instance
        if 'value' in entry and entry['value']['type'] != 'immediate':
            vduid = entry['value']['duid']
            if vduid not in self._datastructures:
                cls = DS_TYPES[entry['value']['type']]
                instance = cls(log=self, duid=vduid, subscribe=False)
                self._datastructures[vduid] = instance
        self._datastructures[duid].consume_entry(entry, unsafe=unsafe)
        for s in self._subscribers:
            s.consume_entry(entry, unsafe=unsafe)

    def _prune(self):
        pass

    def subscribe(self, subscriber, duid):
        if duid is None:
            self._subscribers.append(subscriber)
        else:
            if duid in self._datastructures:
                raise KeyError('A datastructure with this duid already exists.')
            self._datastructures[duid] = subscriber

    def get_datastructure(self, duid):
        return self._datastructures.get(duid)

    def consume_entry(self, entry, unsafe: bool):
        self._consume(entry, unsafe)

    @property
    def performance(self) -> float:
        return self.rcount / (len(self) + 1)

    @property
    def redundancy(self) -> float:
        reallen = sum([len(x) for x in self._datastructures.values()])
        return len(self.sliding_window) / reallen if reallen > 0 else 1
