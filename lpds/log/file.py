
import os
import json

from lpds.log.log import AppendOnlyLog


class AppendOnlyLogFile(AppendOnlyLog):

    def __init__(self, filename: str):
        super().__init__()
        self._filename: str = filename
        self._log = []
        if os.path.exists(self._filename):
            with open(self._filename, 'a+') as f:
                self._log = json.load(f)
        self._serial: int = len(self._log)
        if len(self._log) > 0 and self._log[-1]['serial'] != self._serial:
            raise RuntimeError(f'serial {self._log[-1]["serial"]} does not match expected {self._serial}')

    def __len__(self) -> int:
        return self._serial

    def __getitem__(self, index: int):
        return self._log[index]

    def _append(self, entry) -> int:
        serial = self._serial
        self._serial += 1
        self._log.append(entry)
        with open(self._filename, 'w') as f:
            json.dump(self._log, f)
        for s in self._subscribers:
            s.consume_entry(entry)
        return serial
