import json

from lpds.log.log import AppendOnlyLog


class InMemoryAppendOnlyLog(AppendOnlyLog):

    def __init__(self, *args, **kwargs):
        self._log = []
        self._logbase = 0
        self._serial: int = 0
        super().__init__(*args, **kwargs)

    def __len__(self) -> int:
        return self._serial

    def __getitem__(self, index: int):
        if isinstance(index, slice):
            index = slice(
                index.start - self._logbase if index.start else None,
                index.stop - self._logbase if index.stop else None,
                index.step if index.step else None
            )
            if index.start and index.start < 0 or index.stop and index.stop < 0:
                raise KeyError('Log entry has been pruned')
            return self._log[index]
        else:
            if index < self._logbase:
                raise KeyError('Log entry has been pruned')
            return self._log[index - self._logbase]

    def __iter__(self):
        return iter(self._log)

    def oldest(self) -> int:
        return self._logbase

    def _append(self, entry):
        serial = self._serial
        self._serial += 1
        self._log.append(entry)
        self._consume(entry)
        super()._append(entry)
        self.jstats.append(len(json.dumps(self._log)))
        return serial

    def _prune(self):
        # print(f'prune [{self._logbase}:{self._oldest}]')
        to_delete = self._oldest - self._logbase
        del self._log[:to_delete]
        self._logbase = self._oldest

    def _replicate(self, replicate: AppendOnlyLog):
        for entry in replicate:
            self._log.append(entry)
            self._consume(entry, unsafe=True)
            self._serial = entry['serial']
