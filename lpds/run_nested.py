import string
from random import randint, choice

from lpds.ds.linkedlist import LinkedListRO, LinkedListRW
from lpds.ds.dictionary import DictionaryRO, DictionaryRW
from lpds.ds.set import SetRO, SetRW
from lpds.log.memory import InMemoryAppendOnlyLog
from lpds.ds.util import LpdsJsonEncoder

encoder = LpdsJsonEncoder()

log = InMemoryAppendOnlyLog()

root = LinkedListRW(log)
root.insert(0, 42)
dict1 = DictionaryRW(log)
root.insert(1, dict1)
dict1['foo'] = 23
root.insert(2, 'foobar')
dict1['bar'] = 13.37
dict1['baz'] = LinkedListRW(log)
dict1['baz'].append(None)
dict1['baz'].append(2)
dict1['baz'].append(True)
dict1['qux'] = SetRW(log)
dict1['qux'].add(DictionaryRW(log))
dict1['qux'].add(LinkedListRW(log))
# del root[0]
# root[1] = 42

print(encoder.encode(root))
repl = InMemoryAppendOnlyLog(replicate=log)
roroot = repl.get_datastructure(0)
print(encoder.encode(roroot))
