import string
from random import randint, choice

from lpds.ds.avltree import AVLTreeRO, AVLTreeRW
from lpds.log.memory import InMemoryAppendOnlyLog

from matplotlib import pyplot as plot
from tqdm import tqdm


if __name__ == '__main__':

    log = InMemoryAppendOnlyLog()
    log1 = InMemoryAppendOnlyLog(replicate=log)

    ds0 = AVLTreeRW(log)

    for _ in tqdm(range(10000)):
        if randint(0, 3) == 0 and len(ds0) > 0:
            k = choice(list(ds0._nodes.values()))
            ds0.delete(k.value)
        else:
            k = randint(0, 100000)
            if ds0.search(k) is None:
                ds0.insert(k)
            else:
                ds0.delete(k)

    log2 = InMemoryAppendOnlyLog(replicate=log)

    for _ in tqdm(range(10000)):
        if randint(0, 3) == 0 and len(ds0) > 0:
            k = choice(list(ds0._nodes.values()))
            ds0.delete(k.value)
        else:
            k = randint(0, 100000)
            if ds0.search(k) is None:
                ds0.insert(k)
            else:
                ds0.delete(k)

    log3 = InMemoryAppendOnlyLog(replicate=log)

    ds1 = log1.get_datastructure(0)
    ds2 = log2.get_datastructure(0)
    ds3 = log3.get_datastructure(0)

    print(ds0.oldest())

    print(len(ds0))
    print(ds0 == ds1, ds1.consumed)
    print(ds0 == ds2, ds2.consumed)
    print(ds0 == ds3, ds3.consumed)
    print(log.stats[-1][2])  # performance

    plot.plot(log.stats)
    plot.hlines([1, 1.9], colors=['forestgreen', 'red'], xmin=0, xmax=len(log))
    plot.ylim(bottom=0)
    plot.show()

    plot.plot([x[0]/x[1] if x[1] > 0 else 1 for x in log.stats])
    plot.plot([x[2] for x in log.stats], color='purple')
    plot.hlines([1, 1.9], colors=['forestgreen', 'red'], xmin=0, xmax=len(log))
    plot.ylim(bottom=0, top=4)
    plot.show()

    # plot.hist(ds0._nodes.keys(), bins=25, range=(0, len(log)))
    # plot.axvline(log.oldest(), color='red')
    xs = []
    ys = []
    cs = []
    s = []
    for i in range(log.oldest()):
        xs.append(i // 150)
        ys.append(i % 150)
        s.append(4)
        cs.append('red')
    for i in log.sliding_window:
        if i not in ds0._nodes.keys():
            xs.append(i // 150)
            ys.append(i % 150)
            s.append(4)
            cs.append('yellow')
    for i in log.sliding_window:
        if i in ds0._nodes.keys():
            xs.append(i // 150)
            ys.append(i % 150)
            s.append(4)
            cs.append('blue')
    plot.scatter(xs, ys, s=s, c=cs)
    plot.show()
