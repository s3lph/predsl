import os
import string
from random import randint, choice

from lpds.ds.dictionary import DictionaryRO, DictionaryRW
from lpds.log.memory import InMemoryAppendOnlyLog
from lpds.log.tinyssb import TinySsbSlidingWindowAppendOnlyLog

from matplotlib import pyplot as plot
from tqdm import tqdm

from tinyssb import keystore, util, repository, packet

if __name__ == '__main__':

    os.system("rm -rf data")

    nodes = {}
    for nm in ['Alice', 'Bob']: # create IDs
        ks = keystore.Keystore()
        pk = ks.new(nm)
        nodes[pk] = {
            'name':     nm,
            'feedID':   pk,
            'keystore': ks
        }
    alias = {util.hex(pk):n['name'] for pk,n in nodes.items()}
    for n in nodes.values():
        n['alias'] = alias

    # create genesis entry and add mutual trust anchors
    for pk, n in nodes.items():
        pfx = './data/' + n['name']
        os.system(f"mkdir -p {pfx}/_blob")
        os.system(f"mkdir -p {pfx}/_logs")
        with open(f"{pfx}/config.json", "w") as f:
            f.write(util.json_pp({ 'name': n['name'],
                                   'feedID': util.hex(pk),
                                   'alias': n['alias']}))
        ks = n['keystore']
        with open(f"{pfx}/keystore.json", "w") as f:
            f.write(str(ks))
        repo = repository.REPO(pfx, lambda fid,msg,sig: ks.verify(fid,msg,sig))
        feed = repo.mk_generic_log(n['feedID'], packet.PKTTYPE_plain48,
                                   b'log entry 1', lambda msg: ks.sign(pk, msg))
        for other in nodes.keys(): # install mutual trust
            if other != n['feedID']:
                repo.allocate_log(other, 0, other[:20]) # install trust anchor

    os.system("find data|sort")
